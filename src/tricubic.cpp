#include <iostream>
#include "tricubic.hpp"
#include <cmath>
//This code is adapted from https://github.com/deepzot/likely
TriCubicInterpolator::TriCubicInterpolator(py::list data, py::list data_x, py::list data_y, py::list data_z, py::list npoints)
{

  _n1 = py::cast<int>(npoints[0]);
  _n2 = py::cast<int>(npoints[1]);
  _n3 = py::cast<int>(npoints[2]);
  // std::cout<<"Grid size :"<<_n1<<" "<<_n2<<" "<<_n3<<" "<<std::endl;

  _data_ptr = new fptype[_n1 * _n2 * _n3];
  _data_ptrx = new fptype[_n1 * _n2 * _n3];
  _data_ptry = new fptype[_n1 * _n2 * _n3];
  _data_ptrz = new fptype[_n1 * _n2 * _n3];
  for (int i = 0; i < _n1; i++)
  {
    py::list l1 = data[i];
    for (int j = 0; j < _n2; j++)
    {
      py::list l2 = l1[j];
      for (int k = 0; k < _n3; k++)
        _data_ptr[_index(i, j, k)] = py::cast<fptype>(l2[k]);
    }
  }
  for (int i = 0; i < _n1; i++)
  {
    py::list l1x = data_x[i];
    for (int j = 0; j < _n2; j++)
    {
      py::list l2x = l1x[j];
      for (int k = 0; k < _n3; k++)
      {
        _data_ptrx[_index(i, j, k)] = py::cast<fptype>(l2x[k]);
        // std::cout<<"x points values at: "<< _index(i, j, k)<<" "<<_data_ptrx[_index(i, j, k)]<<std::endl;


      }
    }
  }
  for (int i = 0; i < _n1; i++)
  {
    py::list l1y = data_y[i];
    for (int j = 0; j < _n2; j++)
    {
      py::list l2y = l1y[j];
      for (int k = 0; k < _n3; k++)
        _data_ptry[_index(i, j, k)] = py::cast<fptype>(l2y[k]);
    }
  }
  for (int i = 0; i < _n1; i++)
  {
    py::list l1z = data_z[i];
    for (int j = 0; j < _n2; j++)
    {
      py::list l2z = l1z[j];
      for (int k = 0; k < _n3; k++)
        _data_ptrz[_index(i, j, k)] = py::cast<fptype>(l2z[k]);
    }
  }




  _spacingx = _data_ptrx[_index(1,0,0)]-_data_ptrx[_index(0,0,0)];
  _spacingy = _data_ptry[_index(0,1,0)]-_data_ptry[_index(0,0,0)];
  _spacingz = _data_ptrz[_index(0,0,1)]-_data_ptrz[_index(0,0,0)];
  std::cout<<"Spacing x, y and z are: "<<_spacingx<<" "<<_spacingy<<" "<<_spacingz<<" "<<std::endl;

  //temporary array is necessary, otherwise compiler has problems with Eigen and takes very long to compile
  const int temp[64][64] =
     {{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-3, 3, 0, 0, 0, 0, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {2, -2, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {9, -9, -9, 9, 0, 0, 0, 0, 6, 3, -6, -3, 0, 0, 0, 0, 6, -6, 3, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-6, 6, 6, -6, 0, 0, 0, 0, -3, -3, 3, 3, 0, 0, 0, 0, -4, 4, -2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -2, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-6, 6, 6, -6, 0, 0, 0, 0, -4, -2, 4, 2, 0, 0, 0, 0, -3, 3, -3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -1, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {4, -4, -4, 4, 0, 0, 0, 0, 2, 2, -2, -2, 0, 0, 0, 0, 2, -2, 2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, 0, 0, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, -9, -9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 3, -6, -3, 0, 0, 0, 0, 6, -6, 3, -3, 0, 0, 0, 0, 4, 2, 2, 1, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6, 6, 6, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, -3, 3, 3, 0, 0, 0, 0, -4, 4, -2, 2, 0, 0, 0, 0, -2, -2, -1, -1, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6, 6, 6, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, -2, 4, 2, 0, 0, 0, 0, -3, 3, -3, 3, 0, 0, 0, 0, -2, -1, -2, -1, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, -4, -4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, -2, -2, 0, 0, 0, 0, 2, -2, 2, -2, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0},
      {-3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {9, -9, 0, 0, -9, 9, 0, 0, 6, 3, 0, 0, -6, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, -6, 0, 0, 3, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 2, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-6, 6, 0, 0, 6, -6, 0, 0, -3, -3, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 4, 0, 0, -2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -2, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, -1, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, -9, 0, 0, -9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 3, 0, 0, -6, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, -6, 0, 0, 3, -3, 0, 0, 4, 2, 0, 0, 2, 1, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6, 6, 0, 0, 6, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, -3, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 4, 0, 0, -2, 2, 0, 0, -2, -2, 0, 0, -1, -1, 0, 0},
      {9, 0, -9, 0, -9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 3, 0, -6, 0, -3, 0, 6, 0, -6, 0, 3, 0, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 2, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 9, 0, -9, 0, -9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 3, 0, -6, 0, -3, 0, 6, 0, -6, 0, 3, 0, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 2, 0, 2, 0, 1, 0},
      {-27, 27, 27, -27, 27, -27, -27, 27, -18, -9, 18, 9, 18, 9, -18, -9, -18, 18, -9, 9, 18, -18, 9, -9, -18, 18, 18, -18, -9, 9, 9, -9, -12, -6, -6, -3, 12, 6, 6, 3, -12, -6, 12, 6, -6, -3, 6, 3, -12, 12, -6, 6, -6, 6, -3, 3, -8, -4, -4, -2, -4, -2, -2, -1},
      {18, -18, -18, 18, -18, 18, 18, -18, 9, 9, -9, -9, -9, -9, 9, 9, 12, -12, 6, -6, -12, 12, -6, 6, 12, -12, -12, 12, 6, -6, -6, 6, 6, 6, 3, 3, -6, -6, -3, -3, 6, 6, -6, -6, 3, 3, -3, -3, 8, -8, 4, -4, 4, -4, 2, -2, 4, 4, 2, 2, 2, 2, 1, 1},
      {-6, 0, 6, 0, 6, 0, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, -3, 0, 3, 0, 3, 0, -4, 0, 4, 0, -2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -2, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, -6, 0, 6, 0, 6, 0, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, -3, 0, 3, 0, 3, 0, -4, 0, 4, 0, -2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -2, 0, -1, 0, -1, 0},
      {18, -18, -18, 18, -18, 18, 18, -18, 12, 6, -12, -6, -12, -6, 12, 6, 9, -9, 9, -9, -9, 9, -9, 9, 12, -12, -12, 12, 6, -6, -6, 6, 6, 3, 6, 3, -6, -3, -6, -3, 8, 4, -8, -4, 4, 2, -4, -2, 6, -6, 6, -6, 3, -3, 3, -3, 4, 2, 4, 2, 2, 1, 2, 1},
      {-12, 12, 12, -12, 12, -12, -12, 12, -6, -6, 6, 6, 6, 6, -6, -6, -6, 6, -6, 6, 6, -6, 6, -6, -8, 8, 8, -8, -4, 4, 4, -4, -3, -3, -3, -3, 3, 3, 3, 3, -4, -4, 4, 4, -2, -2, 2, 2, -4, 4, -4, 4, -2, 2, -2, 2, -2, -2, -2, -2, -1, -1, -1, -1},
      {2, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {-6, 6, 0, 0, 6, -6, 0, 0, -4, -2, 0, 0, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, -3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, -1, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {4, -4, 0, 0, -4, 4, 0, 0, 2, 2, 0, 0, -2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6, 6, 0, 0, 6, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, -2, 0, 0, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, -3, 3, 0, 0, -2, -1, 0, 0, -2, -1, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, -4, 0, 0, -4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, -2, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 2, -2, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0},
      {-6, 0, 6, 0, 6, 0, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 0, -2, 0, 4, 0, 2, 0, -3, 0, 3, 0, -3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, -2, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, -6, 0, 6, 0, 6, 0, -6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 0, -2, 0, 4, 0, 2, 0, -3, 0, 3, 0, -3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, 0, -2, 0, -1, 0},
      {18, -18, -18, 18, -18, 18, 18, -18, 12, 6, -12, -6, -12, -6, 12, 6, 12, -12, 6, -6, -12, 12, -6, 6, 9, -9, -9, 9, 9, -9, -9, 9, 8, 4, 4, 2, -8, -4, -4, -2, 6, 3, -6, -3, 6, 3, -6, -3, 6, -6, 3, -3, 6, -6, 3, -3, 4, 2, 2, 1, 4, 2, 2, 1},
      {-12, 12, 12, -12, 12, -12, -12, 12, -6, -6, 6, 6, 6, 6, -6, -6, -8, 8, -4, 4, 8, -8, 4, -4, -6, 6, 6, -6, -6, 6, 6, -6, -4, -4, -2, -2, 4, 4, 2, 2, -3, -3, 3, 3, -3, -3, 3, 3, -4, 4, -2, 2, -4, 4, -2, 2, -2, -2, -1, -1, -2, -2, -1, -1},
      {4, 0, -4, 0, -4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, -2, 0, -2, 0, 2, 0, -2, 0, 2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 4, 0, -4, 0, -4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, -2, 0, -2, 0, 2, 0, -2, 0, 2, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0},
      {-12, 12, 12, -12, 12, -12, -12, 12, -8, -4, 8, 4, 8, 4, -8, -4, -6, 6, -6, 6, 6, -6, 6, -6, -6, 6, 6, -6, -6, 6, 6, -6, -4, -2, -4, -2, 4, 2, 4, 2, -4, -2, 4, 2, -4, -2, 4, 2, -3, 3, -3, 3, -3, 3, -3, 3, -2, -1, -2, -1, -2, -1, -2, -1},
      {8, -8, -8, 8, -8, 8, 8, -8, 4, 4, -4, -4, -4, -4, 4, 4, 4, -4, 4, -4, -4, 4, -4, 4, 4, -4, -4, 4, 4, -4, -4, 4, 2, 2, 2, 2, -2, -2, -2, -2, 2, 2, -2, -2, 2, 2, -2, -2, 2, -2, 2, -2, 2, -2, 2, -2, 1, 1, 1, 1, 1, 1, 1, 1}};

  for (int i = 0; i < 64; i++)
    for (int j = 0; j < 64; j++)
      _C(i, j) = temp[i][j];
}

TriCubicInterpolator::~TriCubicInterpolator()
{
  delete _data_ptr;
  delete _data_ptrx;
  delete _data_ptry;
  delete _data_ptrz;
}


fptype TriCubicInterpolator::ip(py::list xyz)
{

  fptype x = py::cast<fptype>(xyz[0]);
  fptype y = py::cast<fptype>(xyz[1]);
  fptype z = py::cast<fptype>(xyz[2]);
  // std::cout<<" x, y and z are: "<<x<<" "<<y<<" "<<z<<" "<<std::endl;
  int xi = (int)floor(x/_spacingx); //calculate lower-bound grid indices
  int yi = (int)floor(y/_spacingy);
  int zi = 0;
  fptype _z = 0.0;

  while ((z-_z)>=0)
  {

    _z = _z + (_data_ptrz[_index(xi,yi,zi+1)]-_data_ptrz[(_index(xi,yi,zi))]);
    zi = zi +1;

    if (zi == _n3)
      break;

  }

  zi = zi -1;



  fptype dx;
  fptype dy;
  fptype dz;
  fptype result = 0;
  // std::cout<<"dx,dy and dz: "<<dx<<" "<<dy<<" "<<dz<<std::endl;
  // std::cout<<"xi, yi and zi: "<<xi<<" "<<yi<<" "<<zi<<std::endl;

  int condition = 0;

  _spacingz = _data_ptrz[_index(xi,yi,zi+1)]-_data_ptrz[_index(xi,yi,zi)];


  if (abs(_data_ptrx[_index(xi,yi,zi)] - x)<0.01 && abs(_data_ptry[_index(xi,yi,zi)] - y)<0.01 && abs(_data_ptrz[_index(xi,yi,zi)]- z)<0.01)
  {

    result = _data_ptr[_index(xi,yi,zi)];
  }

  else
  {
    if(xi==_n1-1)
    {
      xi = xi-1;
    }
    if(yi==_n2-1)
    {
      yi = yi-1;
    }
    dx = x-_data_ptrx[(_index(xi,yi,zi))];
    dy = y-_data_ptry[(_index(xi,yi,zi))];
    dz = z-_data_ptrz[(_index(xi,yi,zi))];
    // Check if we can re-use coefficients from the last interpolation.
    // std::cout<<"you are in main else loop "<<std::endl;

    // condition 1
    if ( (xi == 0) && (yi == 0) && (zi == 0))
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 1 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 1;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[(_index(xi,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[(_index(xi,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1 , zi + 1)] - _data_ptr[_index(xi +1 , yi + 1, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi +1 , yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1 , zi + 2)] - _data_ptr[_index(xi +1 , yi + 1, zi )]),
          // values of d2f/dxdy at each corner.
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi + 1 , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi)] - _data_ptr[_index(xi +2 , yi+2, zi)]-3*_data_ptr[_index(xi + 2,yi,zi)])-3*(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi + 2 , yi+2, zi)]-3*_data_ptr[_index(xi+2,yi,zi)] - 4*_data_ptr[_index(xi  , yi+1, zi)] + _data_ptr[_index(xi  , yi+2, zi)]+ 3*_data_ptr[_index(xi ,yi,zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi)] - _data_ptr[_index(xi + 1 , yi, zi)]) - (_data_ptr[_index(xi + 2 , yi+2, zi)] - _data_ptr[_index(xi +2 , yi, zi)])-3*(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi)] - _data_ptr[_index(xi + 3 , yi, zi)]-_data_ptr[_index(xi,yi+3,zi)] + _data_ptr[_index(xi  , yi, zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi + 1 , yi+2, zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi+1)] - _data_ptr[_index(xi +2 , yi+2, zi+1)]-3*_data_ptr[_index(xi + 2,yi,zi+1)])-3*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+2, zi+1)]-3*_data_ptr[_index(xi+2,yi,zi+1)] - 4*_data_ptr[_index(xi  , yi+1, zi+1)] + _data_ptr[_index(xi  , yi+2, zi+1)]+ 3*_data_ptr[_index(xi ,yi,zi+1)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi+1)] - _data_ptr[_index(xi + 1 , yi, zi+1)]) - (_data_ptr[_index(xi + 2 , yi+2, zi+1)] - _data_ptr[_index(xi +2 , yi, zi+1)])-3*(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi, zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi+1)] - _data_ptr[_index(xi + 3 , yi, zi+1)]-_data_ptr[_index(xi,yi+3,zi+1)] + _data_ptr[_index(xi  , yi, zi+1)]),
        // values of d2f/dxdz at each corner.
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 1 , yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)]) - (4*_data_ptr[_index(xi + 2 , yi, zi+1)] - _data_ptr[_index(xi +2 , yi, zi + 2)]-3*_data_ptr[_index(xi + 2,yi,zi)])-3*(4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *((4*_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+2)]-3*_data_ptr[_index(xi+2,yi,zi)]) - (4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi + 2)]-3*_data_ptr[_index(xi ,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi + 1 , yi+1, zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - (4*_data_ptr[_index(xi + 2 , yi + 1, zi+1)] - _data_ptr[_index(xi +2 , yi +1, zi + 2)]-3*_data_ptr[_index(xi + 2,yi + 1,zi)])-3*(4*_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi+2)]-3*_data_ptr[_index(xi,yi + 1,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *((4*_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+1, zi+2)]-3*_data_ptr[_index(xi+2,yi+1,zi)]) - (4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+1, zi + 2)]-3*_data_ptr[_index(xi ,yi+1,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi, zi+2)] - _data_ptr[_index(xi + 1 , yi, zi+3)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - (4*_data_ptr[_index(xi + 2 , yi, zi+2)] - _data_ptr[_index(xi +2 , yi, zi + 3)]-3*_data_ptr[_index(xi + 2,yi,zi+1)])-3*(4*_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi+3)]-3*_data_ptr[_index(xi,yi,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+1, zi)]) - (_data_ptr[_index(xi + 1 , yi + 1, zi+2)] - _data_ptr[_index(xi +1 , yi +1, zi )])-3*(_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+1, zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)]) - (4*_data_ptr[_index(xi  , yi+2, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi +1, yi+1, zi+1)] - _data_ptr[_index(xi+1 , yi+1, zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - (4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi +1, yi, zi+1)] - _data_ptr[_index(xi +1, yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi)]) - (4*_data_ptr[_index(xi  , yi, zi+1)] - _data_ptr[_index(xi  , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi)]) - (4*_data_ptr[_index(xi+1  , yi, zi+1)] - _data_ptr[_index(xi+1  , yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi , yi+1, zi+2)] - _data_ptr[_index(xi , yi+1, zi)]) - (_data_ptr[_index(xi , yi+2, zi+2)] - _data_ptr[_index(xi  , yi+2, zi)])-3*(_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi+1 , yi+1, zi+2)] - _data_ptr[_index(xi+1 , yi+1, zi)]) - (_data_ptr[_index(xi+1 , yi+2, zi+2)] - _data_ptr[_index(xi+1  , yi+2, zi)])-3*(_data_ptr[_index(xi+1 , yi, zi+2)] - _data_ptr[_index(xi +1, yi, zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi )])),
          0.5   * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi )])),
          0.5   * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi )])),
          0.5   * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi + 2)] + _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi)] + _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]);
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;


    }
    //condition 2
    else if (xi ==0 && yi == 0 && zi > 0 && zi < _n3-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 2 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 2;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi + 1 , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi)] - _data_ptr[_index(xi +2 , yi+2, zi)]-3*_data_ptr[_index(xi + 2,yi,zi)])-3*(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi + 2 , yi+2, zi)]-3*_data_ptr[_index(xi+2,yi,zi)] - 4*_data_ptr[_index(xi  , yi+1, zi)] + _data_ptr[_index(xi  , yi+2, zi)]+ 3*_data_ptr[_index(xi ,yi,zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi)] - _data_ptr[_index(xi + 1 , yi, zi)]) - (_data_ptr[_index(xi + 2 , yi+2, zi)] - _data_ptr[_index(xi +2 , yi, zi)])-3*(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi)] - _data_ptr[_index(xi + 3 , yi, zi)]-_data_ptr[_index(xi,yi+3,zi)] + _data_ptr[_index(xi  , yi, zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi + 1 , yi+2, zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi+1)] - _data_ptr[_index(xi +2 , yi+2, zi+1)]-3*_data_ptr[_index(xi + 2,yi,zi+1)])-3*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+2, zi+1)]-3*_data_ptr[_index(xi+2,yi,zi+1)] - 4*_data_ptr[_index(xi  , yi+1, zi+1)] + _data_ptr[_index(xi  , yi+2, zi+1)]+ 3*_data_ptr[_index(xi ,yi,zi+1)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi+1)] - _data_ptr[_index(xi + 1 , yi, zi+1)]) - (_data_ptr[_index(xi + 2 , yi+2, zi+1)] - _data_ptr[_index(xi +2 , yi, zi+1)])-3*(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi, zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi+1)] - _data_ptr[_index(xi + 3 , yi, zi+1)]-_data_ptr[_index(xi,yi+3,zi+1)] + _data_ptr[_index(xi  , yi, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)]) - 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi + 2, yi+1, zi - 1)]) - 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi )]) - 3*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi , yi, zi )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 2)] - _data_ptr[_index(xi + 2, yi+1, zi )]) - 3*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi , yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi, yi + 1, zi )] -4*_data_ptr[_index(xi, yi - 1, zi + 1)] + _data_ptr[_index(xi,yi-1,zi+2)]+3*_data_ptr[_index(xi,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 1, zi )] -4*_data_ptr[_index(xi+1, yi - 1, zi + 1)] + _data_ptr[_index(xi+1,yi-1,zi+2)]+3*_data_ptr[_index(xi+1,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi, yi + 2, zi )] -4*_data_ptr[_index(xi, yi, zi + 1)] + _data_ptr[_index(xi,yi,zi+2)]+3*_data_ptr[_index(xi,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 2, zi )] -4*_data_ptr[_index(xi+1, yi, zi + 1)] + _data_ptr[_index(xi+1,yi,zi+2)]+3*_data_ptr[_index(xi+1,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi-1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi-1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi-1 )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi-1)])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi-1 )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi-1 )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi-1 )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi-1 )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi-1 )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi-1 )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi-1 )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi, zi-1 )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi + 2, yi, zi - 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5   * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.25  * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi + 2)] + _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi)] + _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]);
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;
          // Remember this voxel for next time.

    }
    // condition 3
    else if (xi ==0 && yi > 0 && zi == 0 && yi < _n2-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 3 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 3;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi +2, yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi +2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi +2, yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi +2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi+2)]-3*_data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi+2)]-3*_data_ptr[_index(xi+1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi+2)]-3*_data_ptr[_index(xi, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi+2)]-3*_data_ptr[_index(xi+1, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi-1,zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi+2,yi-1,zi)]) - 3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi + 2, zi)]-_data_ptr[_index(xi+2,yi,zi)]) - 3*(_data_ptr[_index(xi , yi + 2, zi)]-_data_ptr[_index(xi,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi-1,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi+2,yi-1,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi+1)]-_data_ptr[_index(xi+1,yi,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 2, zi+1)]-_data_ptr[_index(xi+2,yi,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 2, zi+1)]-_data_ptr[_index(xi,yi,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(4*_data_ptr[_index(xi+1, yi, zi + 1)] - _data_ptr[_index(xi+1, yi, zi+2)]-3*_data_ptr[_index(xi+1, yi, zi)]) - (4*_data_ptr[_index(xi+2, yi, zi + 1)] - _data_ptr[_index(xi+2, yi, zi+2)]-3*_data_ptr[_index(xi+2, yi, zi)])-3*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi+2)]-3*_data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((4*_data_ptr[_index(xi+2, yi, zi + 1)] - _data_ptr[_index(xi+2, yi, zi+2)]-3*_data_ptr[_index(xi+2, yi, zi)]) - (4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi+2)]-3*_data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(4*_data_ptr[_index(xi+1, yi+1, zi + 1)] - _data_ptr[_index(xi+1, yi+1, zi+2)]-3*_data_ptr[_index(xi+1, yi+1, zi)]) - (4*_data_ptr[_index(xi+2, yi+1, zi + 1)] - _data_ptr[_index(xi+2, yi+1, zi+2)]-3*_data_ptr[_index(xi+2, yi+1, zi)])-3*(4*_data_ptr[_index(xi, yi+1, zi + 1)] - _data_ptr[_index(xi, yi+1, zi+2)]-3*_data_ptr[_index(xi, yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((4*_data_ptr[_index(xi+2, yi+1, zi + 1)] - _data_ptr[_index(xi+2, yi+1, zi+2)]-3*_data_ptr[_index(xi+2, yi+1, zi)]) - (4*_data_ptr[_index(xi, yi+1, zi + 1)] - _data_ptr[_index(xi, yi+1, zi+2)]-3*_data_ptr[_index(xi, yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi+1, yi, zi + 2)] - _data_ptr[_index(xi+1, yi, zi)]) - (_data_ptr[_index(xi+2, yi, zi + 2)] - _data_ptr[_index(xi+2, yi, zi)])-3*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi+1, yi+1, zi + 2)] - _data_ptr[_index(xi+1, yi+1, zi)]) - (_data_ptr[_index(xi+2, yi+1, zi + 2)] - _data_ptr[_index(xi+2, yi+1, zi)])-3*(_data_ptr[_index(xi, yi+1, zi + 2)] - _data_ptr[_index(xi, yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi, yi + 1, zi )] -4*_data_ptr[_index(xi, yi - 1, zi + 1)] + _data_ptr[_index(xi,yi-1,zi+2)]+3*_data_ptr[_index(xi,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 1, zi )] -4*_data_ptr[_index(xi+1, yi - 1, zi + 1)] + _data_ptr[_index(xi+1,yi-1,zi+2)]+3*_data_ptr[_index(xi+1,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi, yi + 2, zi )] -4*_data_ptr[_index(xi, yi, zi + 1)] + _data_ptr[_index(xi,yi,zi+2)]+3*_data_ptr[_index(xi,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 2, zi )] -4*_data_ptr[_index(xi+1, yi, zi + 1)] + _data_ptr[_index(xi+1,yi,zi+2)]+3*_data_ptr[_index(xi+1,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi+1 , yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi+1 )] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi+2, yi + 1, zi)] - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi+2, yi - 1, zi)]) - (_data_ptr[_index(xi , yi + 1, zi+1 )] - _data_ptr[_index(xi, yi + 1, zi)]) + (_data_ptr[_index(xi , yi - 1, zi +1)] - _data_ptr[_index(xi, yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi+1 , yi, zi )]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi+2, yi + 2, zi)] - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi+2, yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi+1 )] - _data_ptr[_index(xi, yi + 2, zi)]) + (_data_ptr[_index(xi , yi , zi +1)] - _data_ptr[_index(xi, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi+1 , yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi+2 )] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi+2, yi + 1, zi)] - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi+2, yi - 1, zi)]) - (_data_ptr[_index(xi , yi + 1, zi+2 )] - _data_ptr[_index(xi, yi + 1, zi)]) + (_data_ptr[_index(xi , yi - 1, zi +2)] - _data_ptr[_index(xi, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi+1 , yi, zi )]) - (_data_ptr[_index(xi , yi + 2, zi+2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi+2 )] - _data_ptr[_index(xi , yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi + 2)] + _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi)] + _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]);
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;


    }

    //condition 4
    else if (xi > 0 && yi == 0 && zi == 0 && xi < _n1-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 4 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 4;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[(_index(xi,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[(_index(xi,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1 , zi + 1)] - _data_ptr[_index(xi +1 , yi + 1, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi +1 , yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1 , zi + 2)] - _data_ptr[_index(xi +1 , yi + 1, zi )]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi)] - _data_ptr[_index(xi+1, yi + 2, zi)]-3*_data_ptr[_index(xi+1, yi, zi)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi)] - _data_ptr[_index(xi-1, yi + 2, zi)]-3*_data_ptr[_index(xi-1, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi)] - _data_ptr[_index(xi+2, yi + 2, zi)]-3*_data_ptr[_index(xi+2, yi, zi)]) - (4*_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi + 2, zi)]-3*_data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi+1, yi + 2, zi+1)]-3*_data_ptr[_index(xi+1, yi, zi+1)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi+1)] - _data_ptr[_index(xi-1, yi + 2, zi+1)]-3*_data_ptr[_index(xi-1, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi+1)] - _data_ptr[_index(xi+2, yi + 2, zi+1)]-3*_data_ptr[_index(xi+2, yi, zi+1)]) - (4*_data_ptr[_index(xi, yi + 1, zi+1)] - _data_ptr[_index(xi, yi + 2, zi+1)]-3*_data_ptr[_index(xi, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi )] - 4*_data_ptr[_index(xi - 1, yi, zi + 1)]+_data_ptr[_index(xi - 1, yi, zi + 2)]+3*_data_ptr[_index(xi - 1, yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi +2 , yi, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi, zi )] - 4*_data_ptr[_index(xi , yi, zi + 1)]+_data_ptr[_index(xi , yi, zi + 2)]+3*_data_ptr[_index(xi , yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi +1 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi )] - 4*_data_ptr[_index(xi - 1, yi+1, zi + 1)]+_data_ptr[_index(xi - 1, yi+1, zi + 2)]+3*_data_ptr[_index(xi - 1, yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi +2 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi+1, zi )] - 4*_data_ptr[_index(xi , yi+1, zi + 1)]+_data_ptr[_index(xi , yi+1, zi + 2)]+3*_data_ptr[_index(xi , yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+1, zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)]) - (4*_data_ptr[_index(xi  , yi+2, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi +1, yi+1, zi+1)] - _data_ptr[_index(xi+1 , yi+1, zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - (4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi +1, yi, zi+1)] - _data_ptr[_index(xi +1, yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi)]) - (4*_data_ptr[_index(xi  , yi, zi+1)] - _data_ptr[_index(xi  , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi)]) - (4*_data_ptr[_index(xi+1  , yi, zi+1)] - _data_ptr[_index(xi+1  , yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi , yi+1, zi+2)] - _data_ptr[_index(xi , yi+1, zi)]) - (_data_ptr[_index(xi , yi+2, zi+2)] - _data_ptr[_index(xi , yi+2, zi)])-3*(_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi+1 , yi+1, zi+2)] - _data_ptr[_index(xi+1 , yi+1, zi)]) - (_data_ptr[_index(xi+1 , yi+2, zi+2)] - _data_ptr[_index(xi+1 , yi+2, zi)])-3*(_data_ptr[_index(xi +1, yi, zi+2)] - _data_ptr[_index(xi+1 , yi, zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi+1)] - _data_ptr[_index(xi - 1, yi + 1, zi)]) + (_data_ptr[_index(xi - 1, yi, zi+1)] - _data_ptr[_index(xi - 1, yi, zi)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi )]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi+1)] - _data_ptr[_index(xi - 1, yi + 2, zi)]) + (_data_ptr[_index(xi - 1, yi, zi+1)] - _data_ptr[_index(xi - 1, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi )]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 2, zi)]) + (_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi+2)] - _data_ptr[_index(xi - 1, yi + 1, zi)]) + (_data_ptr[_index(xi - 1, yi, zi+2)] - _data_ptr[_index(xi - 1, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi )]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi+2)] - _data_ptr[_index(xi - 1, yi + 2, zi)]) + (_data_ptr[_index(xi - 1, yi, zi+2)] - _data_ptr[_index(xi - 1, yi, zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi + 2)] + _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi)] + _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]);
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;
          // Remember this voxel for next time.

    }

    //condition 5
    else if (xi ==0 && yi > 0 && zi > 0 && yi <_n2-2 && zi <_n3-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 5: "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 5;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi-1,zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi+2,yi-1,zi)]) - 3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi + 2, zi)]-_data_ptr[_index(xi+2,yi,zi)]) - 3*(_data_ptr[_index(xi , yi + 2, zi)]-_data_ptr[_index(xi,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi-1,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi+2,yi-1,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi+1)]-_data_ptr[_index(xi+1,yi,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 2, zi+1)]-_data_ptr[_index(xi+2,yi,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 2, zi+1)]-_data_ptr[_index(xi,yi,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)]) - 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi + 2, yi+1, zi - 1)]) - 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi )]) - 3*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi , yi, zi )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 2)] - _data_ptr[_index(xi + 2, yi+1, zi )]) - 3*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi , yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi -1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi+1 , yi - 1, zi-1)]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi-1)]) + (_data_ptr[_index(xi , yi - 1, zi+1 )] - _data_ptr[_index(xi , yi - 1, zi-1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi+2, yi + 1, zi -1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi+2 , yi - 1, zi-1)]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi-1)]) + (_data_ptr[_index(xi , yi - 1, zi+1 )] - _data_ptr[_index(xi , yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi -1)]) - (_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi+1 , yi , zi-1)]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 2, zi-1)]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi-1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi+2, yi + 2, zi -1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi+2 , yi , zi-1)]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 2, zi-1)]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi+1 , yi - 1, zi)]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi - 1, zi+2 )] - _data_ptr[_index(xi , yi - 1, zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi+2, yi + 1, zi)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi+2 , yi - 1, zi)]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi - 1, zi+2 )] - _data_ptr[_index(xi , yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi+1 , yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi+2)] - _data_ptr[_index(xi , yi + 2, zi)]) + (_data_ptr[_index(xi , yi , zi+2 )] - _data_ptr[_index(xi , yi , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi + 2)] + _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi)] + _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]);
      // Convert voxel values and partial derivatives to interpolation coefficients.
      _coefs = _C * x;
    }

    //condition 6
    else if ( xi > 0 && yi == 0 && zi > 0 && xi < _n1-2 && zi < _n3-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 6 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 6;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi)] - _data_ptr[_index(xi+1, yi + 2, zi)]-3*_data_ptr[_index(xi+1, yi, zi)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi)] - _data_ptr[_index(xi-1, yi + 2, zi)]-3*_data_ptr[_index(xi-1, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi)] - _data_ptr[_index(xi+2, yi + 2, zi)]-3*_data_ptr[_index(xi+2, yi, zi)]) - (4*_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi + 2, zi)]-3*_data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi+1, yi + 2, zi+1)]-3*_data_ptr[_index(xi+1, yi, zi+1)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi+1)] - _data_ptr[_index(xi-1, yi + 2, zi+1)]-3*_data_ptr[_index(xi-1, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi+1)] - _data_ptr[_index(xi+2, yi + 2, zi+1)]-3*_data_ptr[_index(xi+2, yi, zi+1)]) - (4*_data_ptr[_index(xi, yi + 1, zi+1)] - _data_ptr[_index(xi, yi + 2, zi+1)]-3*_data_ptr[_index(xi, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) - (_data_ptr[_index(xi, yi +2, zi + 1)]-_data_ptr[_index(xi, yi +2, zi - 1)]) - 3*(_data_ptr[_index(xi, yi , zi+1)]-_data_ptr[_index(xi,yi,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi+1, yi +2, zi + 1)]-_data_ptr[_index(xi+1, yi +2, zi - 1)]) - 3*(_data_ptr[_index(xi+1, yi , zi+1)]-_data_ptr[_index(xi+1,yi,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) - (_data_ptr[_index(xi, yi +2, zi + 2)]-_data_ptr[_index(xi, yi +2, zi )]) - 3*(_data_ptr[_index(xi, yi , zi+2)]-_data_ptr[_index(xi,yi,zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi+1, yi +2, zi + 2)]-_data_ptr[_index(xi+1, yi +2, zi )]) - 3*(_data_ptr[_index(xi+1, yi , zi+2)]-_data_ptr[_index(xi+1,yi,zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi -1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi+1 , yi , zi-1)]) - (_data_ptr[_index(xi-1 , yi + 1, zi+1)] - _data_ptr[_index(xi-1 , yi + 1, zi-1)]) + (_data_ptr[_index(xi-1 , yi , zi+1 )] - _data_ptr[_index(xi-1 , yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi+2, yi + 1, zi -1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi+2 , yi , zi-1)]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi-1)]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi-1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi -1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi+1 , yi , zi-1)]) - (_data_ptr[_index(xi-1 , yi + 2, zi+1)] - _data_ptr[_index(xi-1 , yi + 2, zi-1)]) + (_data_ptr[_index(xi-1 , yi , zi+1 )] - _data_ptr[_index(xi-1 , yi , zi-1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi+2, yi + 2, zi -1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi+2 , yi , zi-1)]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 2, zi-1)]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi+1 , yi , zi)]) - (_data_ptr[_index(xi-1 , yi + 1, zi+2)] - _data_ptr[_index(xi-1 , yi + 1, zi)]) + (_data_ptr[_index(xi-1 , yi , zi+2 )] - _data_ptr[_index(xi-1 , yi , zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi+2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi+2 , yi , zi)]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi , zi+2 )] - _data_ptr[_index(xi , yi , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi+1 , yi , zi)]) - (_data_ptr[_index(xi-1 , yi + 2, zi+2)] - _data_ptr[_index(xi-1 , yi + 2, zi)]) + (_data_ptr[_index(xi-1 , yi , zi+2 )] - _data_ptr[_index(xi-1 , yi , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi+2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi+2 , yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi+2)] - _data_ptr[_index(xi , yi + 2, zi)]) + (_data_ptr[_index(xi , yi , zi+2 )] - _data_ptr[_index(xi , yi , zi)]));
         // Convert voxel values and partial derivatives to interpolation coefficients.
        _coefs = _C * x;
    }

    //condition 7
    else if ( xi > 0 && yi > 0 && zi == 0 && xi <_n1-2 && yi < _n2-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K  in condition 7: "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 7;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.

          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi+2)]-3*_data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi+2)]-3*_data_ptr[_index(xi+1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi+2)]-3*_data_ptr[_index(xi, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi+2)]-3*_data_ptr[_index(xi+1, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi+1, yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi)] -4*_data_ptr[_index(xi - 1, yi, zi+1)]+_data_ptr[_index(xi-1,yi,zi+2)]+3*_data_ptr[(_index(xi-1,yi,zi))]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi+2, yi, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi, zi)] -4*_data_ptr[_index(xi , yi, zi+1)]+_data_ptr[_index(xi,yi,zi+2)]+3*_data_ptr[(_index(xi,yi,zi))]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi+1, yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi)] -4*_data_ptr[_index(xi - 1, yi+1, zi+1)]+_data_ptr[_index(xi-1,yi+1,zi+2)]+3*_data_ptr[(_index(xi-1,yi+1,zi))]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi+2, yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi+1, zi)] -4*_data_ptr[_index(xi , yi+1, zi+1)]+_data_ptr[_index(xi,yi+1,zi+2)]+3*_data_ptr[(_index(xi,yi+1,zi))]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi , yi+1, zi + 1)] - _data_ptr[_index(xi, yi+1, zi + 2)] - 3*_data_ptr[_index(xi, yi +1, zi)] -4*_data_ptr[_index(xi , yi-1, zi+1)]+_data_ptr[_index(xi,yi-1,zi+2)]+3*_data_ptr[(_index(xi,yi-1,zi))]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1 , yi+1, zi + 1)] - _data_ptr[_index(xi+1, yi+1, zi + 2)] - 3*_data_ptr[_index(xi+1, yi +1, zi)] -4*_data_ptr[_index(xi+1 , yi-1, zi+1)]+_data_ptr[_index(xi+1,yi-1,zi+2)]+3*_data_ptr[(_index(xi+1,yi-1,zi))]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi , yi+2, zi + 1)] - _data_ptr[_index(xi, yi+2, zi + 2)] - 3*_data_ptr[_index(xi, yi +2, zi)] -4*_data_ptr[_index(xi , yi, zi+1)]+_data_ptr[_index(xi,yi,zi+2)]+3*_data_ptr[(_index(xi,yi,zi))]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1 , yi+2, zi + 1)] - _data_ptr[_index(xi+1, yi+2, zi + 2)] - 3*_data_ptr[_index(xi+1, yi +2, zi)] -4*_data_ptr[_index(xi+1 , yi, zi+1)]+_data_ptr[_index(xi+1,yi,zi+2)]+3*_data_ptr[(_index(xi+1,yi,zi))]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi-1 , zi + 1)] - _data_ptr[_index(xi+1 , yi-1 , zi)]) - (_data_ptr[_index(xi-1 , yi + 1, zi+1)] - _data_ptr[_index(xi-1 , yi + 1, zi)]) + (_data_ptr[_index(xi-1 , yi-1 , zi+1 )] - _data_ptr[_index(xi-1 , yi-1 , zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi+2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi-1 , zi + 1)] - _data_ptr[_index(xi+2 , yi-1 , zi)]) - (_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi-1 , zi+1 )] - _data_ptr[_index(xi , yi-1 , zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi+1 , yi , zi)]) - (_data_ptr[_index(xi-1 , yi + 2, zi+1)] - _data_ptr[_index(xi-1 , yi + 1, zi)]) + (_data_ptr[_index(xi-1 , yi , zi+1 )] - _data_ptr[_index(xi-1 , yi , zi)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi+2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi+2 , yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi+1)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi , zi+1 )] - _data_ptr[_index(xi , yi , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi+1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi-1 , zi + 2)] - _data_ptr[_index(xi+1 , yi-1 , zi)]) - (_data_ptr[_index(xi-1 , yi + 1, zi+2)] - _data_ptr[_index(xi-1 , yi + 1, zi)]) + (_data_ptr[_index(xi-1 , yi-1 , zi+2 )] - _data_ptr[_index(xi-1 , yi-1 , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi+2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi-1 , zi + 2)] - _data_ptr[_index(xi+2 , yi-1 , zi)]) - (_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi-1 , zi+2 )] - _data_ptr[_index(xi , yi-1 , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi+1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi+1 , yi , zi)]) - (_data_ptr[_index(xi-1 , yi + 2, zi+2)] - _data_ptr[_index(xi-1 , yi + 2, zi)]) + (_data_ptr[_index(xi-1 , yi , zi+2 )] - _data_ptr[_index(xi-1 , yi , zi)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi+2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi+2 , yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi+2)] - _data_ptr[_index(xi , yi + 2, zi)]) + (_data_ptr[_index(xi , yi , zi+2 )] - _data_ptr[_index(xi , yi , zi)]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;
    }

    //condition 8
    else if ( xi == _n1-2 && yi == _n2-2 && zi == _n3-2)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 8 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 8;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+1, yi - 1, zi)]) - 4*(_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]) + (_data_ptr[_index(xi-1, yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)])-4*(3*_data_ptr[_index(xi , yi + 1, zi)] - 4*_data_ptr[_index(xi , yi , zi)] + _data_ptr[_index(xi , yi-1, zi)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi+1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)])-4*(3*_data_ptr[_index(xi , yi + 1, zi+1)] - 4*_data_ptr[_index(xi , yi , zi+1)] + _data_ptr[_index(xi , yi-1, zi+1)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi-1, yi, zi+1)] - _data_ptr[_index(xi-1, yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi+1 , zi-1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi+1 , zi-1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi-1, yi+1 , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi , zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi-1 , yi , zi+1)] - 4*_data_ptr[_index(xi-1, yi , zi)] + _data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi - 1, yi+1 , zi)] + _data_ptr[_index(xi - 1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi +1, zi+1)] - 4*_data_ptr[_index(xi, yi +1, zi)] + _data_ptr[_index(xi, yi+1, zi-1)])+(3*_data_ptr[_index(xi-1 , yi +1, zi+1)] - 4*_data_ptr[_index(xi-1, yi+1 , zi)] + _data_ptr[_index(xi-1, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi , yi-1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi+1 , yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) + (_data_ptr[_index(xi +1, yi-1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi , yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi, yi+1, zi-1)]) - (3*_data_ptr[_index(xi , yi-1 , zi+1)] - 4*_data_ptr[_index(xi , yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1 , yi+1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi+1 , zi)] + _data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi+1 , yi-1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi, yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi , yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi, yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi , yi -1, zi+1)] - 4*_data_ptr[_index(xi, yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi+1, yi , zi+1)] - 4*_data_ptr[_index(xi+1, yi , zi)] + _data_ptr[_index(xi+1, yi, zi-1)])+(3*_data_ptr[_index(xi+1 , yi -1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
         (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;
    }

    //condition 9
    else if ( xi == _n1-2 && yi == _n2-2 && zi < _n3-2 && zi >0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 9 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 9;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+1, yi - 1, zi)]) - 4*(_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]) + (_data_ptr[_index(xi-1, yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)])-4*(3*_data_ptr[_index(xi , yi + 1, zi)] - 4*_data_ptr[_index(xi , yi , zi)] + _data_ptr[_index(xi , yi-1, zi)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi+1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)])-4*(3*_data_ptr[_index(xi , yi + 1, zi+1)] - 4*_data_ptr[_index(xi , yi , zi+1)] + _data_ptr[_index(xi , yi-1, zi+1)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 1)]- _data_ptr[_index(xi+1, yi, zi -1 )])  - 4*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + _data_ptr[_index(xi-1, yi, zi +1 )]-_data_ptr[_index(xi-1,yi,zi-1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]- _data_ptr[_index(xi+1, yi+1, zi -1 )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) + _data_ptr[_index(xi-1, yi+1, zi +1 )]-_data_ptr[_index(xi-1,yi+1,zi-1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 2)]- _data_ptr[_index(xi+1, yi, zi )])  - 4*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + _data_ptr[_index(xi-1, yi, zi +2 )]-_data_ptr[_index(xi-1,yi,zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]- _data_ptr[_index(xi+1, yi+1, zi )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) + _data_ptr[_index(xi-1, yi+1, zi +2 )]-_data_ptr[_index(xi-1,yi+1,zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + (_data_ptr[_index(xi, yi-1 , zi + 1)]-_data_ptr[_index(xi,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 1)]-_data_ptr[_index(xi+1,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 2)]-_data_ptr[_index(xi , yi+1, zi)]) - 4*(_data_ptr[_index(xi, yi , zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + (_data_ptr[_index(xi, yi-1 , zi + 2)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 2)]-_data_ptr[_index(xi+1,yi-1,zi)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 10
    else if ( xi == _n1-2 && yi < _n2-2 && zi == _n3-2 && yi>0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 10: "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 10;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)]-_data_ptr[_index(xi + 1, yi-1, zi)]) - 4*(_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi, yi-1, zi)]) + 3*(_data_ptr[_index(xi -1, yi+1 , zi)]-_data_ptr[_index(xi-1,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi)]-_data_ptr[_index(xi + 1, yi, zi)]) - 4*(_data_ptr[_index(xi, yi+2, zi)]-_data_ptr[_index(xi, yi, zi)]) + 3*(_data_ptr[_index(xi -1, yi+2, zi)]-_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)]-_data_ptr[_index(xi + 1, yi-1, zi+1)]) - 4*(_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi, yi-1, zi+1)]) + 3*(_data_ptr[_index(xi -1, yi+1 , zi+1)]-_data_ptr[_index(xi-1,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi+1)]-_data_ptr[_index(xi + 1, yi, zi+1)]) - 4*(_data_ptr[_index(xi, yi+2, zi+1)]-_data_ptr[_index(xi, yi, zi+1)]) + 3*(_data_ptr[_index(xi -1, yi+2, zi+1)]-_data_ptr[_index(xi-1,yi,zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi-1, yi, zi+1)] - _data_ptr[_index(xi-1, yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi+1 , zi-1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi+1 , zi-1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi-1, yi+1 , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi , zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi-1 , yi , zi+1)] - 4*_data_ptr[_index(xi-1, yi , zi)] + _data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi - 1, yi+1 , zi)] + _data_ptr[_index(xi - 1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi +1, zi+1)] - 4*_data_ptr[_index(xi, yi +1, zi)] + _data_ptr[_index(xi, yi+1, zi-1)])+(3*_data_ptr[_index(xi-1 , yi +1, zi+1)] - 4*_data_ptr[_index(xi-1, yi+1 , zi)] + _data_ptr[_index(xi-1, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi)]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi - 1, zi + 1)]-4*_data_ptr[_index(xi, yi - 1, zi)]+_data_ptr[_index(xi, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi)]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi - 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi - 1, zi)]+_data_ptr[_index(xi+1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi)]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi)]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi)]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi)]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 11
    else if ( xi < _n1-2 && yi == _n2-2 && zi == _n3-2 && xi>0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 11 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 11;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] - _data_ptr[_index(xi + 1, yi-2, zi)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi)] + 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi + 2, yi , zi)] - _data_ptr[_index(xi + 2, yi-2, zi)]- 3*_data_ptr[_index(xi , yi+1 , zi)] + 4*_data_ptr[_index(xi , yi , zi)] + _data_ptr[_index(xi , yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] - _data_ptr[_index(xi + 1, yi-2, zi+1)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] + 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 2, yi , zi+1)] - _data_ptr[_index(xi + 2, yi-2, zi+1)]- 3*_data_ptr[_index(xi , yi+1 , zi+1)] + 4*_data_ptr[_index(xi , yi , zi+1)] + _data_ptr[_index(xi , yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]) - (3*_data_ptr[_index(xi-1, yi, zi + 1)] - 4*_data_ptr[_index(xi-1, yi, zi)]+_data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi, zi + 1)] - 4*_data_ptr[_index(xi+2, yi, zi)]+_data_ptr[_index(xi+2, yi, zi-1)]) - (3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi-1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi-1, yi+1, zi)]+_data_ptr[_index(xi-1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+2, yi+1, zi)]+_data_ptr[_index(xi+2, yi+1, zi-1)]) - (3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi , yi-1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi+1 , yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) + (_data_ptr[_index(xi +1, yi-1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi , yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi, yi+1, zi-1)]) - (3*_data_ptr[_index(xi , yi-1 , zi+1)] - 4*_data_ptr[_index(xi , yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1 , yi+1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi+1 , zi)] + _data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi+1 , yi-1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi , yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi , yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi, yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi , yi -1, zi+1)] - 4*_data_ptr[_index(xi, yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi+1, yi , zi+1)] - 4*_data_ptr[_index(xi+1, yi , zi)] + _data_ptr[_index(xi+1, yi, zi-1)])+(3*_data_ptr[_index(xi+1 , yi -1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;
    }

    //condition 12
    else if ( xi == _n1-2 && yi < _n2-2 && zi < _n3-2 && yi>0 && zi > 0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 12 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 12;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)]-_data_ptr[_index(xi + 1, yi-1, zi)]) - 4*(_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi, yi-1, zi)]) + 3*(_data_ptr[_index(xi -1, yi+1 , zi)]-_data_ptr[_index(xi-1,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi)]-_data_ptr[_index(xi + 1, yi, zi)]) - 4*(_data_ptr[_index(xi, yi+2, zi)]-_data_ptr[_index(xi, yi, zi)]) + 3*(_data_ptr[_index(xi -1, yi+2, zi)]-_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)]-_data_ptr[_index(xi + 1, yi-1, zi+1)]) - 4*(_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi, yi-1, zi+1)]) + 3*(_data_ptr[_index(xi -1, yi+1 , zi+1)]-_data_ptr[_index(xi-1,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi+1)]-_data_ptr[_index(xi + 1, yi, zi+1)]) - 4*(_data_ptr[_index(xi, yi+2, zi+1)]-_data_ptr[_index(xi, yi, zi+1)]) + 3*(_data_ptr[_index(xi -1, yi+2, zi+1)]-_data_ptr[_index(xi-1,yi,zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 1)]- _data_ptr[_index(xi+1, yi, zi -1 )])  - 4*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + _data_ptr[_index(xi-1, yi, zi +1 )]-_data_ptr[_index(xi-1,yi,zi-1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]- _data_ptr[_index(xi+1, yi+1, zi -1 )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) + _data_ptr[_index(xi-1, yi+1, zi +1 )]-_data_ptr[_index(xi-1,yi+1,zi-1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 2)]- _data_ptr[_index(xi+1, yi, zi )])  - 4*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + _data_ptr[_index(xi-1, yi, zi +2 )]-_data_ptr[_index(xi-1,yi,zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]- _data_ptr[_index(xi+1, yi+1, zi )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) + _data_ptr[_index(xi-1, yi+1, zi +2 )]-_data_ptr[_index(xi-1,yi+1,zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 2)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }


    //condition 13
    else if ( xi < _n1-2 && yi == _n2-2 && zi < _n3-2 && xi>0 && zi >0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 13 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 13;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-2, zi)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi)] + 4*_data_ptr[_index(xi - 1, yi , zi)] - _data_ptr[_index(xi - 1, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi + 2, yi , zi)] + _data_ptr[_index(xi + 2, yi-2, zi)]- 3*_data_ptr[_index(xi , yi+1 , zi)] + 4*_data_ptr[_index(xi , yi , zi)] - _data_ptr[_index(xi , yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-2, zi+1)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] + 4*_data_ptr[_index(xi - 1, yi , zi+1)] - _data_ptr[_index(xi - 1, yi-1, zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 2, yi , zi+1)] + _data_ptr[_index(xi + 2, yi-2, zi+1)]- 3*_data_ptr[_index(xi , yi+1 , zi+1)] + 4*_data_ptr[_index(xi , yi , zi+1)] - _data_ptr[_index(xi , yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + (_data_ptr[_index(xi, yi-1 , zi + 1)]-_data_ptr[_index(xi,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 1)]-_data_ptr[_index(xi+1,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) - 4*(_data_ptr[_index(xi, yi , zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + (_data_ptr[_index(xi, yi-1 , zi + 2)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 2)]-_data_ptr[_index(xi+1,yi-1,zi)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }


    //condition 14
    else if ( xi < _n1-2 && yi < _n2-2 && zi == _n3-2 && xi>0 && yi>0)
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 14 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 14;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]) - (3*_data_ptr[_index(xi-1, yi, zi + 1)] - 4*_data_ptr[_index(xi-1, yi, zi)]+_data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi, zi + 1)] - 4*_data_ptr[_index(xi+2, yi, zi)]+_data_ptr[_index(xi+2, yi, zi-1)]) - (3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi-1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi-1, yi+1, zi)]+_data_ptr[_index(xi-1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+2, yi+1, zi)]+_data_ptr[_index(xi+2, yi+1, zi-1)]) - (3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi)]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi - 1, zi + 1)]-4*_data_ptr[_index(xi, yi - 1, zi)]+_data_ptr[_index(xi, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi)]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi - 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi - 1, zi)]+_data_ptr[_index(xi+1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi)]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi)]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi)]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi)]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
        // Convert voxel values and partial derivatives to interpolation coefficients.
        _coefs = _C * x;

    }

    //condition 15
    else if ( xi == _n1-2 && yi == 0 && zi == 0 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 15 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 15;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[(_index(xi,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[(_index(xi,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1 , zi + 1)] - _data_ptr[_index(xi +1 , yi + 1, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi +1 , yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1 , zi + 2)] - _data_ptr[_index(xi +1 , yi + 1, zi )]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi)]+_data_ptr[_index(xi-1,yi+2,zi)]+ 3*_data_ptr[_index(xi-1,yi,zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi+2,zi)]-3*_data_ptr[_index(xi,yi,zi)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] -3*_data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi+1)]+_data_ptr[_index(xi-1,yi+2,zi+1)]+ 3*_data_ptr[_index(xi-1,yi,zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi+2,zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - _data_ptr[_index(xi - 1, yi + 2, zi+1)] -3*_data_ptr[_index(xi - 1, yi + 1, zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi )] - 4*_data_ptr[_index(xi - 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*(4*_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi+2)]-3*_data_ptr[_index(xi,yi,zi)])+ (4*_data_ptr[_index(xi-1, yi, zi + 1)]-_data_ptr[_index(xi-1,yi,zi+2)]-3*_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi + 1, yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi )] - 4*_data_ptr[_index(xi - 1, yi+1, zi + 1)] + _data_ptr[_index(xi - 1, yi+1, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(4*_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)])+ (4*_data_ptr[_index(xi-1, yi+1, zi + 1)]-_data_ptr[_index(xi-1,yi+1,zi+2)]-3*_data_ptr[_index(xi-1,yi+1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+1, zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)]) - (4*_data_ptr[_index(xi  , yi+2, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(4*_data_ptr[_index(xi +1, yi+1, zi+1)] - _data_ptr[_index(xi+1 , yi+1, zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - (4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi+2)])-3*(4*_data_ptr[_index(xi +1, yi, zi+1)] - _data_ptr[_index(xi +1, yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi+2, zi+2)]-3*_data_ptr[_index(xi,yi+2,zi)]) - (4*_data_ptr[_index(xi  , yi, zi+1)] - _data_ptr[_index(xi  , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *((4*_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi+2, zi+2)]-3*_data_ptr[_index(xi+1,yi+2,zi)]) - (4*_data_ptr[_index(xi+1  , yi, zi+1)] - _data_ptr[_index(xi+1  , yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi , yi+1, zi+2)] - _data_ptr[_index(xi , yi+1, zi)]) - (_data_ptr[_index(xi , yi+2, zi+2)] - _data_ptr[_index(xi  , yi+2, zi)])-3*(_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingy*_spacingz)) *(4*(_data_ptr[_index(xi+1 , yi+1, zi+2)] - _data_ptr[_index(xi+1 , yi+1, zi)]) - (_data_ptr[_index(xi+1 , yi+2, zi+2)] - _data_ptr[_index(xi+1  , yi+2, zi)])-3*(_data_ptr[_index(xi+1 , yi, zi+2)] - _data_ptr[_index(xi +1, yi, zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi  )])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi  )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi  )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi  )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 2)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 16
    else if ( xi == 0 && yi == _n2-2 && zi == 0 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 16 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 16;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[(_index(xi,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[(_index(xi,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1 , zi + 1)] - _data_ptr[_index(xi +1 , yi + 1, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi +1 , yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1 , zi + 2)] - _data_ptr[_index(xi +1 , yi + 1, zi )]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi + 1, yi - 1, zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi + 2, yi - 1, zi)]) -3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi , yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi)]-4*_data_ptr[_index(xi+1,yi,zi)]+_data_ptr[_index(xi+1,yi-1,zi)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi)]-4*_data_ptr[_index(xi+2,yi,zi)]+_data_ptr[_index(xi+2,yi-1,zi)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi)]-4*_data_ptr[_index(xi,yi,zi)]+_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi+2, yi , zi)] + _data_ptr[_index(xi + 2, yi-1, zi)] - 3*_data_ptr[_index(xi , yi + 1, zi)] + 4*_data_ptr[_index(xi, yi-1 , zi)] - _data_ptr[_index(xi, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi + 1, yi - 1, zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi + 2, yi - 1, zi+1)]) -3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi , yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)]-4*_data_ptr[_index(xi+1,yi,zi+1)]+_data_ptr[_index(xi+1,yi-1,zi+1)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi+1)]-4*_data_ptr[_index(xi+2,yi,zi+1)]+_data_ptr[_index(xi+2,yi-1,zi+1)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi+1)]-4*_data_ptr[_index(xi,yi,zi+1)]+_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+2, yi , zi+1)] + _data_ptr[_index(xi + 2, yi-1, zi+1)] - 3*_data_ptr[_index(xi , yi + 1, zi+1)] + 4*_data_ptr[_index(xi, yi-1 , zi+1)] - _data_ptr[_index(xi, yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 1 , yi, zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)]) - (4*_data_ptr[_index(xi + 2 , yi, zi+1)] - _data_ptr[_index(xi +2 , yi, zi + 2)]-3*_data_ptr[_index(xi + 2,yi,zi)])-3*(4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi+2)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *((4*_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+2)]-3*_data_ptr[_index(xi+2,yi,zi)]) - (4*_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi , yi, zi + 2)]-3*_data_ptr[_index(xi ,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi + 1 , yi+1, zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - (4*_data_ptr[_index(xi + 2 , yi + 1, zi+1)] - _data_ptr[_index(xi +2 , yi +1, zi + 2)]-3*_data_ptr[_index(xi + 2,yi + 1,zi)])-3*(4*_data_ptr[_index(xi , yi + 1, zi+1)] - _data_ptr[_index(xi , yi + 1, zi+2)]-3*_data_ptr[_index(xi,yi + 1,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *((4*_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+1, zi+2)]-3*_data_ptr[_index(xi+2,yi+1,zi)]) - (4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+1, zi + 2)]-3*_data_ptr[_index(xi ,yi+1,zi)])),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(4*_data_ptr[_index(xi + 1, yi, zi+2)] - _data_ptr[_index(xi + 1 , yi, zi+3)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - (4*_data_ptr[_index(xi + 2 , yi, zi+2)] - _data_ptr[_index(xi +2 , yi, zi + 3)]-3*_data_ptr[_index(xi + 2,yi,zi+1)])-3*(4*_data_ptr[_index(xi , yi, zi+2)] - _data_ptr[_index(xi , yi, zi+3)]-3*_data_ptr[_index(xi,yi,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 *(1.0/(_spacingx*_spacingz)) *(4*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+1, zi)]) - (_data_ptr[_index(xi + 1 , yi + 1, zi+2)] - _data_ptr[_index(xi +1 , yi +1, zi )])-3*(_data_ptr[_index(xi , yi + 1, zi+2)] - _data_ptr[_index(xi , yi + 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi , yi+1, zi + 1)] - _data_ptr[_index(xi , yi+1, zi + 2)] - 3*_data_ptr[_index(xi , yi+1, zi )] - 4*_data_ptr[_index(xi , yi-1, zi + 1)]+_data_ptr[_index(xi , yi-1, zi + 2)]+3*_data_ptr[_index(xi , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1 , yi+1, zi + 1)] - _data_ptr[_index(xi+1 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi+1 , yi+1, zi )] - 4*_data_ptr[_index(xi +1, yi-1, zi + 1)]+_data_ptr[_index(xi+1 , yi-1, zi + 2)]+3*_data_ptr[_index(xi+1 , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[_index(xi, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[_index(xi, yi , zi )])+(4*_data_ptr[_index(xi, yi -1, zi + 1)]-_data_ptr[_index(xi, yi -1, zi + 2)]-3*_data_ptr[_index(xi, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi + 2)]-3*_data_ptr[_index(xi+1, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi, zi + 2)]-3*_data_ptr[_index(xi+1, yi , zi )])+(4*_data_ptr[_index(xi+1, yi -1, zi + 1)]-_data_ptr[_index(xi+1, yi -1, zi + 2)]-3*_data_ptr[_index(xi+1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi + 1, zi + 2)]-_data_ptr[_index(xi, yi + 1, zi )]) - 4*(_data_ptr[_index(xi, yi , zi + 2)]-_data_ptr[_index(xi, yi, zi )])+(_data_ptr[_index(xi, yi -1, zi + 2)]-_data_ptr[_index(xi, yi -1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi + 1, zi + 2)]-_data_ptr[_index(xi+1, yi + 1, zi )]) - 4*(_data_ptr[_index(xi+1, yi , zi + 2)]-_data_ptr[_index(xi+1, yi, zi )])+(_data_ptr[_index(xi+1, yi -1, zi + 2)]-_data_ptr[_index(xi+1, yi -1, zi )])),
            // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 17
    else if ( xi == 0 && yi == 0 && zi == _n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 17 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 17;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi + 1 , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi)] - _data_ptr[_index(xi +2 , yi+2, zi)]-3*_data_ptr[_index(xi + 2,yi,zi)])-3*(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi + 2 , yi+2, zi)]-3*_data_ptr[_index(xi+2,yi,zi)] - 4*_data_ptr[_index(xi  , yi+1, zi)] + _data_ptr[_index(xi  , yi+2, zi)]+ 3*_data_ptr[_index(xi ,yi,zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi)] - _data_ptr[_index(xi + 1 , yi, zi)]) - (_data_ptr[_index(xi + 2 , yi+2, zi)] - _data_ptr[_index(xi +2 , yi, zi)])-3*(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi , yi, zi)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi)] - _data_ptr[_index(xi + 3 , yi, zi)]-_data_ptr[_index(xi,yi+3,zi)] + _data_ptr[_index(xi  , yi, zi)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi + 1 , yi+2, zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - (4*_data_ptr[_index(xi + 2 , yi+1, zi+1)] - _data_ptr[_index(xi +2 , yi+2, zi+1)]-3*_data_ptr[_index(xi + 2,yi,zi+1)])-3*(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi + 2 , yi+2, zi+1)]-3*_data_ptr[_index(xi+2,yi,zi+1)] - 4*_data_ptr[_index(xi  , yi+1, zi+1)] + _data_ptr[_index(xi  , yi+2, zi+1)]+ 3*_data_ptr[_index(xi ,yi,zi+1)]),
          0.25 *(1.0/(_spacingx*_spacingy)) *(4*(_data_ptr[_index(xi + 1, yi+2, zi+1)] - _data_ptr[_index(xi + 1 , yi, zi+1)]) - (_data_ptr[_index(xi + 2 , yi+2, zi+1)] - _data_ptr[_index(xi +2 , yi, zi+1)])-3*(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi , yi, zi+1)])),
          0.25 *(1.0/(_spacingx*_spacingy)) *(_data_ptr[_index(xi + 3, yi+3, zi+1)] - _data_ptr[_index(xi + 3 , yi, zi+1)]-_data_ptr[_index(xi,yi+3,zi+1)] + _data_ptr[_index(xi  , yi, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi + 1, yi, zi - 1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)]-_data_ptr[_index(xi + 2, yi, zi - 1)])- 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi + 1, yi+1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)]-_data_ptr[_index(xi + 2, yi+1, zi - 1)])- 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi, zi + 1)]-4*_data_ptr[_index(xi + 1, yi, zi )]+_data_ptr[_index(xi + 1, yi, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )])-3*(3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi , yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )]) - (3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi, yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 1, yi+1, zi )]+_data_ptr[_index(xi + 1, yi+1, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )])-3*(3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi , yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )]) - (3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi, yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi - 1)]) - (_data_ptr[_index(xi, yi + 2, zi + 1)]-_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi+1, yi + 2, zi + 1)]-_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi )]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi )]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi, zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 18
    else if ( xi == 0 && yi == _n2-2 && zi == _n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 18 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 18;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi + 2 , yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi)] - _data_ptr[_index(xi, yi+1, zi)]),
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi + 2 , yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi+1)] - _data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi+1, zi+1)] - _data_ptr[_index(xi, yi+1, zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi-1, zi)]+_data_ptr[_index(xi, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi-1, zi)]+_data_ptr[_index(xi+1, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi-1, zi+1)]+_data_ptr[_index(xi, yi-2, zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1, zi+1)]+_data_ptr[_index(xi+1, yi-2, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi + 1, yi - 1, zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi + 2, yi - 1, zi)]) -3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi , yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi)]-4*_data_ptr[_index(xi+1,yi,zi)]+_data_ptr[_index(xi+1,yi-1,zi)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi)]-4*_data_ptr[_index(xi+2,yi,zi)]+_data_ptr[_index(xi+2,yi-1,zi)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi)]-4*_data_ptr[_index(xi,yi,zi)]+_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi+2, yi , zi)] + _data_ptr[_index(xi + 2, yi-1, zi)] - 3*_data_ptr[_index(xi , yi + 1, zi)] + 4*_data_ptr[_index(xi, yi-1 , zi)] - _data_ptr[_index(xi, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi + 1, yi - 1, zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi + 2, yi - 1, zi+1)]) -3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi , yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)]-4*_data_ptr[_index(xi+1,yi,zi+1)]+_data_ptr[_index(xi+1,yi-1,zi+1)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi+1)]-4*_data_ptr[_index(xi+2,yi,zi+1)]+_data_ptr[_index(xi+2,yi-1,zi+1)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi+1)]-4*_data_ptr[_index(xi,yi,zi+1)]+_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+2, yi , zi+1)] + _data_ptr[_index(xi + 2, yi-1, zi+1)] - 3*_data_ptr[_index(xi , yi + 1, zi+1)] + 4*_data_ptr[_index(xi, yi-1 , zi+1)] - _data_ptr[_index(xi, yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi + 1, yi, zi - 1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)]-_data_ptr[_index(xi + 2, yi, zi - 1)])- 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi + 1, yi+1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)]-_data_ptr[_index(xi + 2, yi+1, zi - 1)])- 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi, zi + 1)]-4*_data_ptr[_index(xi + 1, yi, zi )]+_data_ptr[_index(xi + 1, yi, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )])-3*(3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi , yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )]) - (3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi, yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 1, yi+1, zi )]+_data_ptr[_index(xi + 1, yi+1, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )])-3*(3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi , yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )]) - (3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi, yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi , yi-1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1, yi + 1, zi-1)]) - 4*(_data_ptr[_index(xi+1 , yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) + (_data_ptr[_index(xi +1, yi-1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi , yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi, yi+1, zi-1)]) - (3*_data_ptr[_index(xi , yi-1 , zi+1)] - 4*_data_ptr[_index(xi , yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1 , yi+1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi+1 , zi)] + _data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi+1 , yi-1 , zi+1)] - 4*_data_ptr[_index(xi +1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi , yi+1 , zi+1)] - 4*_data_ptr[_index(xi , yi+1 , zi)] + _data_ptr[_index(xi , yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi, yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi , yi -1, zi+1)] - 4*_data_ptr[_index(xi, yi-1 , zi)] + _data_ptr[_index(xi, yi-1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi+1, yi , zi+1)] - 4*_data_ptr[_index(xi+1, yi , zi)] + _data_ptr[_index(xi+1, yi, zi-1)])+(3*_data_ptr[_index(xi+1 , yi -1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1 , zi)] + _data_ptr[_index(xi+1, yi-1, zi-1)])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    //condition 19
    else if ( xi == _n1-2 && yi == 0 && zi == _n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 19 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 19;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi  , yi+2, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi)] - _data_ptr[_index(xi  , yi, zi)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi)] - _data_ptr[_index(xi+1 , yi, zi)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi  , yi+2, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(4*_data_ptr[_index(xi+1 , yi+1, zi+1)] - _data_ptr[_index(xi+1  , yi+2, zi)]-3*_data_ptr[_index(xi+1,yi,zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi , yi+2, zi+1)] - _data_ptr[_index(xi  , yi, zi+1)]),
          0.5 *(1.0/_spacingy) *(_data_ptr[_index(xi+1 , yi+2, zi+1)] - _data_ptr[_index(xi+1 , yi, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi)]+_data_ptr[_index(xi-1,yi+2,zi)]+ 3*_data_ptr[_index(xi-1,yi,zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi+2,zi)]-3*_data_ptr[_index(xi,yi,zi)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] -3*_data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi+1)]+_data_ptr[_index(xi-1,yi+2,zi+1)]+ 3*_data_ptr[_index(xi-1,yi,zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi+2,zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - _data_ptr[_index(xi - 1, yi + 2, zi+1)] -3*_data_ptr[_index(xi - 1, yi + 1, zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi+1, yi , zi-1)]) - 4*(_data_ptr[_index(xi , yi, zi+1)] - _data_ptr[_index(xi, yi , zi-1)]) + (_data_ptr[_index(xi-1, yi, zi+1)] - _data_ptr[_index(xi-1, yi , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi+1 , zi-1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi+1 , zi-1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi-1, yi+1 , zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi , zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi , zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi , zi+1)] - 4*_data_ptr[_index(xi, yi , zi)] + _data_ptr[_index(xi, yi, zi-1)])+(3*_data_ptr[_index(xi-1 , yi , zi+1)] - 4*_data_ptr[_index(xi-1, yi , zi)] + _data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi - 1, yi+1 , zi)] + _data_ptr[_index(xi - 1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi+1 , zi+1)] - 4*_data_ptr[_index(xi + 1, yi+1 , zi)] + _data_ptr[_index(xi + 1, yi+1, zi-1)]) - 4*(3*_data_ptr[_index(xi , yi +1, zi+1)] - 4*_data_ptr[_index(xi, yi +1, zi)] + _data_ptr[_index(xi, yi+1, zi-1)])+(3*_data_ptr[_index(xi-1 , yi +1, zi+1)] - 4*_data_ptr[_index(xi-1, yi+1 , zi)] + _data_ptr[_index(xi-1, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi - 1)]) - (_data_ptr[_index(xi, yi + 2, zi + 1)]-_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi+1, yi + 2, zi + 1)]-_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi )]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi )]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

  //condition 20
    else if ( xi == _n1-2 && yi == _n2-2 && zi == 0 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 20;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi-1, zi)]+_data_ptr[_index(xi, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi-1, zi)]+_data_ptr[_index(xi+1, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi-1, zi+1)]+_data_ptr[_index(xi, yi-2, zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1, zi+1)]+_data_ptr[_index(xi+1, yi-2, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[(_index(xi,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[(_index(xi,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1 , zi + 1)] - _data_ptr[_index(xi +1 , yi + 1, zi + 2)]-3*_data_ptr[(_index(xi +1 ,yi + 1,zi))]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi +1 , yi, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi )]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1 , zi + 2)] - _data_ptr[_index(xi +1 , yi + 1, zi )]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi+1, yi - 1, zi)]) - 4*(_data_ptr[_index(xi , yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]) + (_data_ptr[_index(xi-1, yi+1, zi)] - _data_ptr[_index(xi, yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-1, zi)])-4*(3*_data_ptr[_index(xi , yi + 1, zi)] - 4*_data_ptr[_index(xi , yi , zi)] + _data_ptr[_index(xi , yi-1, zi)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi)] - 4*_data_ptr[_index(xi - 1, yi , zi)] + _data_ptr[_index(xi - 1, yi-1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi+1, yi - 1, zi+1)]) - 4*(_data_ptr[_index(xi , yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)]) + (_data_ptr[_index(xi-1, yi+1, zi+1)] - _data_ptr[_index(xi, yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)]) - (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-1, zi+1)])-4*(3*_data_ptr[_index(xi , yi + 1, zi+1)] - 4*_data_ptr[_index(xi , yi , zi+1)] + _data_ptr[_index(xi , yi-1, zi+1)]) + (3*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi - 1, yi , zi+1)] + _data_ptr[_index(xi - 1, yi-1, zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi )] - 4*_data_ptr[_index(xi - 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*(4*_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi+2)]-3*_data_ptr[_index(xi,yi,zi)])+ (4*_data_ptr[_index(xi-1, yi, zi + 1)]-_data_ptr[_index(xi-1,yi,zi+2)]-3*_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi + 1, yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi )] - 4*_data_ptr[_index(xi - 1, yi+1, zi + 1)] + _data_ptr[_index(xi - 1, yi+1, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(4*_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)])+ (4*_data_ptr[_index(xi-1, yi+1, zi + 1)]-_data_ptr[_index(xi-1,yi+1,zi+2)]-3*_data_ptr[_index(xi-1,yi+1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) - 4*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi,yi,zi)])+ (_data_ptr[_index(xi-1, yi, zi + 2)]-_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)])+ (_data_ptr[_index(xi-1, yi+1, zi + 2)]-_data_ptr[_index(xi-1,yi+1,zi)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi , yi+1, zi + 1)] - _data_ptr[_index(xi , yi+1, zi + 2)] - 3*_data_ptr[_index(xi , yi+1, zi )] - 4*_data_ptr[_index(xi , yi-1, zi + 1)]+_data_ptr[_index(xi , yi-1, zi + 2)]+3*_data_ptr[_index(xi , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1 , yi+1, zi + 1)] - _data_ptr[_index(xi+1 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi+1 , yi+1, zi )] - 4*_data_ptr[_index(xi +1, yi-1, zi + 1)]+_data_ptr[_index(xi+1 , yi-1, zi + 2)]+3*_data_ptr[_index(xi+1 , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[_index(xi, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[_index(xi, yi , zi )])+(4*_data_ptr[_index(xi, yi -1, zi + 1)]-_data_ptr[_index(xi, yi -1, zi + 2)]-3*_data_ptr[_index(xi, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi + 2)]-3*_data_ptr[_index(xi+1, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi, zi + 2)]-3*_data_ptr[_index(xi+1, yi , zi )])+(4*_data_ptr[_index(xi+1, yi -1, zi + 1)]-_data_ptr[_index(xi+1, yi -1, zi + 2)]-3*_data_ptr[_index(xi+1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi + 1, zi + 2)]-_data_ptr[_index(xi, yi + 1, zi )]) - 4*(_data_ptr[_index(xi, yi , zi + 2)]-_data_ptr[_index(xi, yi, zi )])+(_data_ptr[_index(xi, yi -1, zi + 2)]-_data_ptr[_index(xi, yi -1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi + 1, zi + 2)]-_data_ptr[_index(xi+1, yi + 1, zi )]) - 4*(_data_ptr[_index(xi+1, yi , zi + 2)]-_data_ptr[_index(xi+1, yi, zi )])+(_data_ptr[_index(xi+1, yi -1, zi + 2)]-_data_ptr[_index(xi+1, yi -1, zi )])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
           (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }
  // condition 21
    else if ( xi == _n1-2 && yi == 0 && zi > 0 && zi<_n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in condition 15 : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
        condition = 21;
        Eigen::Matrix<fptype, 64, 1> x;
        x <<
            // values of f(x,y,z) at each corner.
            _data_ptr[_index(xi, yi, zi)],
            _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
            _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
            _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
            // values of df/dx at each corner.
            0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
            0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]-_data_ptr[_index(xi-1, yi , zi)]),
            0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
            0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi-1, yi+1 , zi)]),
            0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
            0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]-_data_ptr[_index(xi-1, yi , zi+1)]),
            0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
            0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi-1, yi+1 , zi+1)]),
            // values of df/dy at each corner.
            0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi +2, zi)]-3*_data_ptr[_index(xi, yi , zi)]),
            0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi+1, yi + 1, zi)] - _data_ptr[_index(xi+1, yi +2, zi)]-3*_data_ptr[_index(xi+1, yi , zi)]),
            0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
            0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
            0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi, yi + 1, zi+1)] - _data_ptr[_index(xi, yi +2, zi+1)]-3*_data_ptr[_index(xi, yi , zi+1)]),
            0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi+1, yi +2, zi+1)]-3*_data_ptr[_index(xi+1, yi , zi+1)]),
            0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
            0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
            // values of df/dz at each corner.
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
            0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
            // values of d2f/dxdy at each corner.
            0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi)]+_data_ptr[_index(xi-1,yi+2,zi)]+ 3*_data_ptr[_index(xi-1,yi,zi)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi)]-_data_ptr[_index(xi+1,yi+2,zi)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi+2,zi)]-3*_data_ptr[_index(xi,yi,zi)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] -3*_data_ptr[_index(xi - 1, yi + 1, zi)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(4*_data_ptr[_index(xi+1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)] - 4*_data_ptr[_index(xi-1 , yi + 1, zi+1)]+_data_ptr[_index(xi-1,yi+2,zi+1)]+ 3*_data_ptr[_index(xi-1,yi,zi+1)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(3*(4*_data_ptr[_index(xi+1 , yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi+2,zi+1)]-3*_data_ptr[_index(xi+1,yi,zi+1)]) - 4*((4*_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi+2,zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)])) + 4*_data_ptr[_index(xi - 1, yi + 1, zi+1)] - _data_ptr[_index(xi - 1, yi + 2, zi+1)] -3*_data_ptr[_index(xi - 1, yi + 1, zi+1)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
            0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
            // values of d2f/dxdz at each corner.
            0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 1)]- _data_ptr[_index(xi+1, yi, zi -1 )])  - 4*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + _data_ptr[_index(xi-1, yi, zi +1 )]-_data_ptr[_index(xi-1,yi,zi-1)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]- _data_ptr[_index(xi+1, yi+1, zi -1 )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) + _data_ptr[_index(xi-1, yi+1, zi +1 )]-_data_ptr[_index(xi-1,yi+1,zi-1)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 2)]- _data_ptr[_index(xi+1, yi, zi )])  - 4*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + _data_ptr[_index(xi-1, yi, zi +2 )]-_data_ptr[_index(xi-1,yi,zi)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
            0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]- _data_ptr[_index(xi+1, yi+1, zi )])  - 4*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) + _data_ptr[_index(xi-1, yi+1, zi +2 )]-_data_ptr[_index(xi-1,yi+1,zi)]),
            // values of d2f/dydz at each corner.
            0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) - (_data_ptr[_index(xi, yi +2, zi + 1)]-_data_ptr[_index(xi, yi +2, zi - 1)]) - 3*(_data_ptr[_index(xi, yi , zi+1)]-_data_ptr[_index(xi,yi,zi-1)])),
            0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi+1, yi +2, zi + 1)]-_data_ptr[_index(xi+1, yi +2, zi - 1)]) - 3*(_data_ptr[_index(xi+1, yi , zi+1)]-_data_ptr[_index(xi+1,yi,zi-1)])),
            0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
            0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
            0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) - (_data_ptr[_index(xi, yi +2, zi + 2)]-_data_ptr[_index(xi, yi +2, zi )]) - 3*(_data_ptr[_index(xi, yi , zi+2)]-_data_ptr[_index(xi,yi,zi)])),
            0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi+1, yi +2, zi + 2)]-_data_ptr[_index(xi+1, yi +2, zi )]) - 3*(_data_ptr[_index(xi+1, yi , zi+2)]-_data_ptr[_index(xi+1,yi,zi)])),
            0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
            // values of d3f/dxdydz at each corner.
            0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi + 1, yi + 2, zi + 1)]-_data_ptr[_index(xi+1,yi+2,zi-1)]) - 3*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)])
            -4*(_data_ptr[_index(xi - 1, yi + 1, zi + 1)]-_data_ptr[_index(xi-1,yi+1,zi-1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)]-_data_ptr[_index(xi-1,yi+2,zi-1)]) - 3*(_data_ptr[_index(xi - 1, yi, zi + 1)]-_data_ptr[_index(xi-1,yi,zi-1)])),
            (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi )] - _data_ptr[_index(xi + 1, yi , zi + 1)] + _data_ptr[_index(xi+1, yi , zi )] - _data_ptr[_index(xi , yi + 1, zi +1 )] + _data_ptr[_index(xi, yi + 1, zi )] + _data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi, yi , zi )]),
            0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi - 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)] - _data_ptr[_index(xi - 1, yi, zi - 1)]),
            (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi )] - _data_ptr[_index(xi + 1, yi+1 , zi + 1)] + _data_ptr[_index(xi+1, yi+1 , zi )] - _data_ptr[_index(xi , yi + 2, zi +1 )] + _data_ptr[_index(xi, yi + 2, zi )] + _data_ptr[_index(xi , yi+1 , zi + 1)] - _data_ptr[_index(xi, yi +1, zi )]),
            0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi + 1, yi + 2, zi + 2)]-_data_ptr[_index(xi+1,yi+2,zi)]) - 3*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)])
            -4*(_data_ptr[_index(xi - 1, yi + 1, zi + 2)]-_data_ptr[_index(xi-1,yi+1,zi)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 2)]-_data_ptr[_index(xi-1,yi+2,zi)]) - 3*(_data_ptr[_index(xi - 1, yi, zi + 2)]-_data_ptr[_index(xi-1,yi,zi)])),
            (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi + 1, yi , zi + 2)] + _data_ptr[_index(xi+1, yi , zi+1)] - _data_ptr[_index(xi , yi + 1, zi + 2 )] + _data_ptr[_index(xi, yi + 1, zi+1)] + _data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi, yi , zi+1)]),
            0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi - 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] + _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi - 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
            (1.0/(_spacingx*_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi+1, yi + 2, zi+1)] - _data_ptr[_index(xi + 1, yi+1 , zi + 2)] + _data_ptr[_index(xi+1, yi+1 , zi+1)] - _data_ptr[_index(xi , yi + 2, zi +2 )] + _data_ptr[_index(xi, yi + 2, zi+1)] + _data_ptr[_index(xi , yi+1 , zi + 2)] - _data_ptr[_index(xi, yi +1, zi+1)]);
        // Convert voxel values and partial derivatives to interpolation coefficients.
        _coefs = _C * x;

    }

    // condition 22
    else if ( xi == _n1-2 && yi > 0 && yi < _n2-2 && zi ==0 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 22;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi-1, yi , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi-1, yi+1 , zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi-1, yi , zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(3*_data_ptr[_index(xi + 1, yi+1, zi+1)] - 4*_data_ptr[_index(xi, yi+1, zi+1)]+_data_ptr[_index(xi-1, yi+1 , zi+1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi+2)]-3*_data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi+2)]-3*_data_ptr[_index(xi+1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi+2)]-3*_data_ptr[_index(xi, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi+2)]-3*_data_ptr[_index(xi+1, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi)]-_data_ptr[_index(xi + 1, yi-1, zi)]) - 4*(_data_ptr[_index(xi, yi+1, zi)]-_data_ptr[_index(xi, yi-1, zi)]) + (_data_ptr[_index(xi -1, yi+1 , zi)]-_data_ptr[_index(xi-1,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi)]-_data_ptr[_index(xi + 1, yi, zi)]) - 4*(_data_ptr[_index(xi, yi+2, zi)]-_data_ptr[_index(xi, yi, zi)]) + (_data_ptr[_index(xi -1, yi+2, zi)]-_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+1, zi+1)]-_data_ptr[_index(xi + 1, yi-1, zi+1)]) - 4*(_data_ptr[_index(xi, yi+1, zi+1)]-_data_ptr[_index(xi, yi-1, zi+1)]) + (_data_ptr[_index(xi -1, yi+1 , zi+1)]-_data_ptr[_index(xi-1,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*(_data_ptr[_index(xi + 1, yi+2, zi+1)]-_data_ptr[_index(xi + 1, yi, zi+1)]) - 4*(_data_ptr[_index(xi, yi+2, zi+1)]-_data_ptr[_index(xi, yi, zi+1)]) + (_data_ptr[_index(xi -1, yi+2, zi+1)]-_data_ptr[_index(xi-1,yi,zi+1)])),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi )] - 4*_data_ptr[_index(xi - 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi+2)]-3*_data_ptr[_index(xi+1,yi,zi)]) - 4*(4*_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi,yi,zi+2)]-3*_data_ptr[_index(xi,yi,zi)])+ (4*_data_ptr[_index(xi-1, yi, zi + 1)]-_data_ptr[_index(xi-1,yi,zi+2)]-3*_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi + 1, yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi )] - 4*_data_ptr[_index(xi - 1, yi+1, zi + 1)] + _data_ptr[_index(xi - 1, yi+1, zi + 2)] + 3*_data_ptr[_index(xi - 1, yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi+2)]-3*_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(4*_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi,yi+1,zi+2)]-3*_data_ptr[_index(xi,yi+1,zi)])+ (4*_data_ptr[_index(xi-1, yi+1, zi + 1)]-_data_ptr[_index(xi-1,yi+1,zi+2)]-3*_data_ptr[_index(xi-1,yi+1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) - 4*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi,yi,zi)])+ (_data_ptr[_index(xi-1, yi, zi + 2)]-_data_ptr[_index(xi-1,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(3*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi,yi+1,zi)])+ (_data_ptr[_index(xi-1, yi+1, zi + 2)]-_data_ptr[_index(xi-1,yi+1,zi)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi, yi + 1, zi )] -4*_data_ptr[_index(xi, yi - 1, zi + 1)] + _data_ptr[_index(xi,yi-1,zi+2)]+3*_data_ptr[_index(xi,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)] - _data_ptr[_index(xi+1, yi + 1, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 1, zi )] -4*_data_ptr[_index(xi+1, yi - 1, zi + 1)] + _data_ptr[_index(xi+1,yi-1,zi+2)]+3*_data_ptr[_index(xi+1,yi-1,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi, yi + 2, zi )] -4*_data_ptr[_index(xi, yi, zi + 1)] + _data_ptr[_index(xi,yi,zi+2)]+3*_data_ptr[_index(xi,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1, yi + 2, zi + 1)] - _data_ptr[_index(xi+1, yi + 2, zi + 2)] - 3*_data_ptr[_index(xi+1, yi + 2, zi )] -4*_data_ptr[_index(xi+1, yi, zi + 1)] + _data_ptr[_index(xi+1,yi,zi+2)]+3*_data_ptr[_index(xi+1,yi,zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 2)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
      _coefs = _C * x;

    }

    // condition 23
    else if ( xi >0 && xi<_n1-2 && yi == _n2-2 && zi ==0 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 22;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi-1, zi)]+_data_ptr[_index(xi, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi-1, zi)]+_data_ptr[_index(xi+1, yi-2, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi-1, zi+1)]+_data_ptr[_index(xi, yi-2, zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi-1, zi+1)]+_data_ptr[_index(xi+1, yi-2, zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi, zi+1)] -_data_ptr[_index(xi, yi, zi+2)] -3*_data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi+1, yi, zi+1)] -_data_ptr[_index(xi+1, yi, zi+2)] -3*_data_ptr[_index(xi+1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi, yi+1, zi+1)] -_data_ptr[_index(xi, yi+1, zi+2)] -3*_data_ptr[_index(xi, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(4*_data_ptr[_index(xi+1, yi+1, zi+1)] -_data_ptr[_index(xi+1, yi+1, zi+2)] -3*_data_ptr[_index(xi+1, yi+1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi)] - 4*_data_ptr[_index(xi + 1, yi , zi)] + _data_ptr[_index(xi + 1, yi-2, zi)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi)] + 4*_data_ptr[_index(xi - 1, yi , zi)] - _data_ptr[_index(xi - 1, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi + 2, yi , zi)] + _data_ptr[_index(xi + 2, yi-2, zi)]- 3*_data_ptr[_index(xi , yi+1 , zi)] + 4*_data_ptr[_index(xi , yi , zi)] - _data_ptr[_index(xi , yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 1, yi , zi+1)] + _data_ptr[_index(xi + 1, yi-2, zi+1)]- 3*_data_ptr[_index(xi - 1, yi+1 , zi+1)] + 4*_data_ptr[_index(xi - 1, yi , zi+1)] - _data_ptr[_index(xi - 1, yi-1, zi+1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi + 2, yi , zi+1)] + _data_ptr[_index(xi + 2, yi-2, zi+1)]- 3*_data_ptr[_index(xi , yi+1 , zi+1)] + 4*_data_ptr[_index(xi , yi , zi+1)] - _data_ptr[_index(xi , yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi +1 , yi, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi, zi )] - 4*_data_ptr[_index(xi - 1, yi, zi + 1)]+_data_ptr[_index(xi - 1, yi, zi + 2)]+3*_data_ptr[_index(xi - 1, yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi +2 , yi, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi, zi )] - 4*_data_ptr[_index(xi , yi, zi + 1)]+_data_ptr[_index(xi , yi, zi + 2)]+3*_data_ptr[_index(xi , yi, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 1, yi+1, zi + 1)] - _data_ptr[_index(xi +1 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 1, yi+1, zi )] - 4*_data_ptr[_index(xi - 1, yi+1, zi + 1)]+_data_ptr[_index(xi - 1, yi+1, zi + 2)]+3*_data_ptr[_index(xi - 1, yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi +2 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi + 2, yi+1, zi )] - 4*_data_ptr[_index(xi , yi+1, zi + 1)]+_data_ptr[_index(xi , yi+1, zi + 2)]+3*_data_ptr[_index(xi , yi+1, zi )]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi , yi+1, zi + 1)] - _data_ptr[_index(xi , yi+1, zi + 2)] - 3*_data_ptr[_index(xi , yi+1, zi )] - 4*_data_ptr[_index(xi , yi-1, zi + 1)]+_data_ptr[_index(xi , yi-1, zi + 2)]+3*_data_ptr[_index(xi , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*_data_ptr[_index(xi+1 , yi+1, zi + 1)] - _data_ptr[_index(xi+1 , yi+1, zi + 2)] - 3*_data_ptr[_index(xi+1 , yi+1, zi )] - 4*_data_ptr[_index(xi +1, yi-1, zi + 1)]+_data_ptr[_index(xi+1 , yi-1, zi + 2)]+3*_data_ptr[_index(xi+1 , yi-1, zi )]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi + 2)]-3*_data_ptr[_index(xi, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi, zi + 2)]-3*_data_ptr[_index(xi, yi , zi )])+(4*_data_ptr[_index(xi, yi -1, zi + 1)]-_data_ptr[_index(xi, yi -1, zi + 2)]-3*_data_ptr[_index(xi, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(4*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi + 2)]-3*_data_ptr[_index(xi+1, yi + 1, zi )]) - 4*(4*_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi, zi + 2)]-3*_data_ptr[_index(xi+1, yi , zi )])+(4*_data_ptr[_index(xi+1, yi -1, zi + 1)]-_data_ptr[_index(xi+1, yi -1, zi + 2)]-3*_data_ptr[_index(xi+1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    // condition 24
    else if ( xi ==0 && yi == _n2-2 && zi >0 && zi<_n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 24;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi +2, yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi +2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi +2, yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi +2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1,yi-2,zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi, yi + 1, zi+1)] - 4*_data_ptr[_index(xi, yi, zi+1)]+_data_ptr[_index(xi,yi-2,zi+1)]),
          0.5 * (1.0/_spacingy)*(3*_data_ptr[_index(xi+1, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+1, yi, zi+1)]+_data_ptr[_index(xi+1,yi-2,zi+1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi + 1, yi - 1, zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi + 2, yi - 1, zi)]) -3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi , yi - 1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi)]-4*_data_ptr[_index(xi+1,yi,zi)]+_data_ptr[_index(xi+1,yi-1,zi)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi)]-4*_data_ptr[_index(xi+2,yi,zi)]+_data_ptr[_index(xi+2,yi-1,zi)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi)]-4*_data_ptr[_index(xi,yi,zi)]+_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi)] - 4*_data_ptr[_index(xi+2, yi , zi)] + _data_ptr[_index(xi + 2, yi-1, zi)] - 3*_data_ptr[_index(xi , yi + 1, zi)] + 4*_data_ptr[_index(xi, yi-1 , zi)] - _data_ptr[_index(xi, yi-1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi + 1, yi - 1, zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi + 2, yi - 1, zi+1)]) -3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi , yi - 1, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(3*_data_ptr[_index(xi + 1, yi + 1, zi+1)]-4*_data_ptr[_index(xi+1,yi,zi+1)]+_data_ptr[_index(xi+1,yi-1,zi+1)]) - (3*_data_ptr[_index(xi + 2, yi + 1, zi+1)]-4*_data_ptr[_index(xi+2,yi,zi+1)]+_data_ptr[_index(xi+2,yi-1,zi+1)]) -3*(3*_data_ptr[_index(xi , yi + 1, zi+1)]-4*_data_ptr[_index(xi,yi,zi+1)]+_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(3*_data_ptr[_index(xi + 2, yi + 1, zi+1)] - 4*_data_ptr[_index(xi+2, yi , zi+1)] + _data_ptr[_index(xi + 2, yi-1, zi+1)] - 3*_data_ptr[_index(xi , yi + 1, zi+1)] + 4*_data_ptr[_index(xi, yi-1 , zi+1)] - _data_ptr[_index(xi, yi-1, zi+1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)]) - 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)] - _data_ptr[_index(xi + 2, yi+1, zi - 1)]) - 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi )]) - 3*(_data_ptr[_index(xi , yi, zi + 2)]-_data_ptr[_index(xi , yi, zi )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 2)] - _data_ptr[_index(xi + 2, yi+1, zi )]) - 3*(_data_ptr[_index(xi , yi+1, zi + 2)]-_data_ptr[_index(xi , yi+1, zi)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 1)]-_data_ptr[_index(xi,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi,yi,zi-1)]) + (_data_ptr[_index(xi, yi-1 , zi + 1)]-_data_ptr[_index(xi,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 1)]-_data_ptr[_index(xi+1,yi+1,zi-1)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1,yi,zi-1)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 1)]-_data_ptr[_index(xi+1,yi-1,zi-1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi, yi+1 , zi + 2)]-_data_ptr[_index(xi,yi+1,zi)]) - 4*(_data_ptr[_index(xi, yi , zi + 2)]-_data_ptr[_index(xi,yi,zi)]) + (_data_ptr[_index(xi, yi-1 , zi + 2)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(3*(_data_ptr[_index(xi+1, yi+1 , zi + 2)]-_data_ptr[_index(xi+1,yi+1,zi)]) - 4*(_data_ptr[_index(xi+1, yi , zi + 2)]-_data_ptr[_index(xi+1,yi,zi)]) + (_data_ptr[_index(xi+1, yi-1 , zi + 2)]-_data_ptr[_index(xi+1,yi-1,zi)])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi +1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    // condition 25
    else if ( xi ==0 && yi < _n2-2 && yi >0 && zi ==_n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 25;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(4*_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi +2, yi, zi)]-3*_data_ptr[_index(xi,yi,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi)] - _data_ptr[_index(xi +2, yi+1, zi)]-3*_data_ptr[_index(xi,yi+1,zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi, zi+1)] - _data_ptr[_index(xi +2, yi, zi+1)]-3*_data_ptr[_index(xi,yi,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(4*_data_ptr[_index(xi + 1, yi+1, zi+1)] - _data_ptr[_index(xi +2, yi+1, zi+1)]-3*_data_ptr[_index(xi,yi+1,zi+1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi)]-_data_ptr[_index(xi+1,yi-1,zi)]) - (_data_ptr[_index(xi + 2, yi + 1, zi)]-_data_ptr[_index(xi+2,yi-1,zi)]) - 3*(_data_ptr[_index(xi , yi + 1, zi)]-_data_ptr[_index(xi,yi-1,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi)]-_data_ptr[_index(xi+1,yi,zi)]) - (_data_ptr[_index(xi + 2, yi + 2, zi)]-_data_ptr[_index(xi+2,yi,zi)]) - 3*(_data_ptr[_index(xi , yi + 2, zi)]-_data_ptr[_index(xi,yi,zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 1, zi+1)]-_data_ptr[_index(xi+1,yi-1,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 1, zi+1)]-_data_ptr[_index(xi+2,yi-1,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 1, zi+1)]-_data_ptr[_index(xi,yi-1,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(4*(_data_ptr[_index(xi + 1, yi + 2, zi+1)]-_data_ptr[_index(xi+1,yi,zi+1)]) - (_data_ptr[_index(xi + 2, yi + 2, zi+1)]-_data_ptr[_index(xi+2,yi,zi+1)]) - 3*(_data_ptr[_index(xi , yi + 2, zi+1)]-_data_ptr[_index(xi,yi,zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi, zi + 1)]-_data_ptr[_index(xi + 1, yi, zi - 1)]) - (_data_ptr[_index(xi + 2, yi, zi + 1)]-_data_ptr[_index(xi + 2, yi, zi - 1)])- 3*(_data_ptr[_index(xi , yi, zi + 1)]-_data_ptr[_index(xi , yi, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(_data_ptr[_index(xi + 1, yi+1, zi + 1)]-_data_ptr[_index(xi + 1, yi+1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi+1, zi + 1)]-_data_ptr[_index(xi + 2, yi+1, zi - 1)])- 3*(_data_ptr[_index(xi , yi+1, zi + 1)]-_data_ptr[_index(xi , yi+1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi, zi + 1)]-4*_data_ptr[_index(xi + 1, yi, zi )]+_data_ptr[_index(xi + 1, yi, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )])-3*(3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi , yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi, zi + 1)]-4*_data_ptr[_index(xi + 2, yi, zi )]+_data_ptr[_index(xi + 2, yi, zi-1 )]) - (3*_data_ptr[_index(xi , yi, zi + 1)]-4*_data_ptr[_index(xi, yi, zi )]+_data_ptr[_index(xi , yi, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*(4*(3*_data_ptr[_index(xi + 1, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 1, yi+1, zi )]+_data_ptr[_index(xi + 1, yi+1, zi-1 )])- (3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )])-3*(3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi , yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi + 2, yi+1, zi + 1)]-4*_data_ptr[_index(xi + 2, yi+1, zi )]+_data_ptr[_index(xi + 2, yi+1, zi-1 )]) - (3*_data_ptr[_index(xi , yi+1, zi + 1)]-4*_data_ptr[_index(xi, yi+1, zi )]+_data_ptr[_index(xi , yi+1, zi-1 )])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi)]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi - 1, zi + 1)]-4*_data_ptr[_index(xi, yi - 1, zi)]+_data_ptr[_index(xi, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi)]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi - 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi - 1, zi)]+_data_ptr[_index(xi+1, yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi)]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi)]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi)]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi)]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    // condition 26
    else if ( xi>0 && xi < _n1-2 && yi ==0 && zi ==_n3-2 )
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 26;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi + 2, zi)]-3*_data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi+1, yi + 1, zi)] - _data_ptr[_index(xi+1, yi + 2, zi)]-3*_data_ptr[_index(xi+1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi, yi + 1, zi+1)] - _data_ptr[_index(xi, yi + 2, zi+1)]-3*_data_ptr[_index(xi, yi, zi+1)]),
          0.5 * (1.0/_spacingy)*(4*_data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi+1, yi + 2, zi+1)]-3*_data_ptr[_index(xi+1, yi, zi+1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)]),
          0.5 * (1.0/_spacingz)*(3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi)] - _data_ptr[_index(xi+1, yi + 2, zi)]-3*_data_ptr[_index(xi+1, yi, zi)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi)] - _data_ptr[_index(xi-1, yi + 2, zi)]-3*_data_ptr[_index(xi-1, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi)] - _data_ptr[_index(xi+2, yi + 2, zi)]-3*_data_ptr[_index(xi+2, yi, zi)]) - (4*_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi + 2, zi)]-3*_data_ptr[_index(xi, yi, zi)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+1, yi + 1, zi+1)] - _data_ptr[_index(xi+1, yi + 2, zi+1)]-3*_data_ptr[_index(xi+1, yi, zi+1)]) - (4*_data_ptr[_index(xi-1, yi + 1, zi+1)] - _data_ptr[_index(xi-1, yi + 2, zi+1)]-3*_data_ptr[_index(xi-1, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*((4*_data_ptr[_index(xi+2, yi + 1, zi+1)] - _data_ptr[_index(xi+2, yi + 2, zi+1)]-3*_data_ptr[_index(xi+2, yi, zi+1)]) - (4*_data_ptr[_index(xi, yi + 1, zi+1)] - _data_ptr[_index(xi, yi + 2, zi+1)]-3*_data_ptr[_index(xi, yi, zi+1)])),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi, zi + 1)] - 4*_data_ptr[_index(xi+1, yi, zi)]+_data_ptr[_index(xi+1, yi, zi-1)]) - (3*_data_ptr[_index(xi-1, yi, zi + 1)] - 4*_data_ptr[_index(xi-1, yi, zi)]+_data_ptr[_index(xi-1, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi, zi + 1)] - 4*_data_ptr[_index(xi+2, yi, zi)]+_data_ptr[_index(xi+2, yi, zi-1)]) - (3*_data_ptr[_index(xi, yi, zi + 1)] - 4*_data_ptr[_index(xi, yi, zi)]+_data_ptr[_index(xi, yi, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+1, yi+1, zi)]+_data_ptr[_index(xi+1, yi+1, zi-1)]) - (3*_data_ptr[_index(xi-1, yi+1, zi + 1)] - 4*_data_ptr[_index(xi-1, yi+1, zi)]+_data_ptr[_index(xi-1, yi+1, zi-1)])),
          0.25 * (1.0/(_spacingx*_spacingz))*((3*_data_ptr[_index(xi+2, yi+1, zi + 1)] - 4*_data_ptr[_index(xi+2, yi+1, zi)]+_data_ptr[_index(xi+2, yi+1, zi-1)]) - (3*_data_ptr[_index(xi, yi+1, zi + 1)] - 4*_data_ptr[_index(xi, yi+1, zi)]+_data_ptr[_index(xi, yi+1, zi-1)])),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi, yi + 1, zi + 1)]-_data_ptr[_index(xi, yi + 1, zi - 1)]) - (_data_ptr[_index(xi, yi + 2, zi + 1)]-_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi, yi , zi + 1)]-_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(_data_ptr[_index(xi+1, yi + 1, zi + 1)]-_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi+1, yi + 2, zi + 1)]-_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(_data_ptr[_index(xi+1, yi , zi + 1)]-_data_ptr[_index(xi+1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi, yi + 1, zi + 1)]-4*_data_ptr[_index(xi, yi + 1, zi )]+_data_ptr[_index(xi, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*(4*(3*_data_ptr[_index(xi+1, yi + 1, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 1, zi )]+_data_ptr[_index(xi+1, yi + 1, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) -3*(3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi -1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi, yi + 2, zi + 1)]-4*_data_ptr[_index(xi, yi + 2, zi )]+_data_ptr[_index(xi, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi, yi , zi + 1)]-4*_data_ptr[_index(xi, yi , zi )]+_data_ptr[_index(xi, yi , zi - 1)])),
          0.25 * (1.0/(_spacingy*_spacingz))*((3*_data_ptr[_index(xi+1, yi + 2, zi + 1)]-4*_data_ptr[_index(xi+1, yi + 2, zi )]+_data_ptr[_index(xi+1, yi + 2, zi - 1)]) - (3*_data_ptr[_index(xi+1, yi , zi + 1)]-4*_data_ptr[_index(xi+1, yi , zi )]+_data_ptr[_index(xi+1, yi , zi - 1)])),
          // values of d3f/dxdydz at each corner.
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi )] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi , zi )] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi )] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi , zi )] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.5 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi )] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi , zi )] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 1, zi )] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi , zi )] - _data_ptr[_index(xi , yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi )] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi )] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi )] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.25 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi )] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi )] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi )] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi )] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

  // condition 27
    else
    {
      // Extract the local vocal values and calculate partial derivatives.
      // std::cout<<" i , j and K in else : "<<xi<<" "<<yi<<" "<<zi<<" "<<std::endl;
      condition = 27;
      Eigen::Matrix<fptype, 64, 1> x;
      x <<
          // values of f(x,y,z) at each corner.
          _data_ptr[_index(xi, yi, zi)],
          _data_ptr[_index(xi + 1, yi, zi)], _data_ptr[_index(xi, yi + 1, zi)],
          _data_ptr[_index(xi + 1, yi + 1, zi)], _data_ptr[_index(xi, yi, zi + 1)], _data_ptr[_index(xi + 1, yi, zi + 1)],
          _data_ptr[_index(xi, yi + 1, zi + 1)], _data_ptr[_index(xi + 1, yi + 1, zi + 1)],
          // values of df/dx at each corner.
          0.5 *(1.0/_spacingx) *(_data_ptr[_index(xi + 1, yi, zi)] - _data_ptr[_index(xi - 1, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)]),
          0.5 * (1.0/_spacingx)*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)]),
          // values of df/dy at each corner.
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)]),
          0.5 * (1.0/_spacingy)*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)]),
          // values of df/dz at each corner.
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)]),
          0.5 * (1.0/_spacingz)*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)]),
          // values of d2f/dxdy at each corner.
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi)] - _data_ptr[_index(xi - 1, yi + 1, zi)] - _data_ptr[_index(xi + 1, yi - 1, zi)] + _data_ptr[_index(xi - 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi)] - _data_ptr[_index(xi, yi + 1, zi)] - _data_ptr[_index(xi + 2, yi - 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi)] - _data_ptr[_index(xi - 1, yi + 2, zi)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi)] - _data_ptr[_index(xi, yi + 2, zi)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] + _data_ptr[_index(xi - 1, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi + 1)] + _data_ptr[_index(xi, yi - 1, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] + _data_ptr[_index(xi - 1, yi, zi + 1)]),
          0.25 * (1.0/(_spacingx*_spacingy))*(_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi + 1)] + _data_ptr[_index(xi, yi, zi + 1)]),
          // values of d2f/dxdz at each corner.
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi - 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi - 1)] + _data_ptr[_index(xi - 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi + 2, yi, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi - 1, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi + 1, zi - 1)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi - 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi)] + _data_ptr[_index(xi - 1, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi + 2, yi, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi - 1, yi + 1, zi)]),
          0.25 * (1.0/(_spacingx*_spacingz))*(_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi)] + _data_ptr[_index(xi, yi + 1, zi)]),
          // values of d2f/dydz at each corner.
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 1)] - _data_ptr[_index(xi, yi - 1, zi + 1)] - _data_ptr[_index(xi, yi + 1, zi - 1)] + _data_ptr[_index(xi, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)] + _data_ptr[_index(xi + 1, yi - 1, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 1)] - _data_ptr[_index(xi, yi, zi + 1)] - _data_ptr[_index(xi, yi + 2, zi - 1)] + _data_ptr[_index(xi, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)] + _data_ptr[_index(xi + 1, yi, zi - 1)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 1, zi + 2)] - _data_ptr[_index(xi, yi - 1, zi + 2)] - _data_ptr[_index(xi, yi + 1, zi)] + _data_ptr[_index(xi, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi)] + _data_ptr[_index(xi + 1, yi - 1, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi, yi + 2, zi + 2)] - _data_ptr[_index(xi, yi, zi + 2)] - _data_ptr[_index(xi, yi + 2, zi)] + _data_ptr[_index(xi, yi, zi)]),
          0.25 * (1.0/(_spacingy*_spacingz))*(_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi)] + _data_ptr[_index(xi + 1, yi, zi)]),
          // values of d3f/dxdydz at each corner.
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 1)] - _data_ptr[_index(xi + 1, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 1)] - _data_ptr[_index(xi + 1, yi - 1, zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 1)] - _data_ptr[_index(xi - 1, yi + 1, zi - 1)]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 1)] - _data_ptr[_index(xi - 1, yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 1)] - _data_ptr[_index(xi + 2, yi + 1, zi - 1)]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 1)] - _data_ptr[_index(xi + 2, yi - 1, zi - 1)]) - (_data_ptr[_index(xi , yi + 1, zi + 1)] - _data_ptr[_index(xi , yi + 1, zi - 1)]) + (_data_ptr[_index(xi , yi - 1, zi + 1)] - _data_ptr[_index(xi , yi - 1, zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 1)] - _data_ptr[_index(xi + 1, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 1, yi , zi + 1)] - _data_ptr[_index(xi + 1, yi , zi - 1)]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 1)] - _data_ptr[_index(xi - 1, yi + 2, zi - 1)]) + (_data_ptr[_index(xi - 1, yi , zi + 1)] - _data_ptr[_index(xi - 1, yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 1)] - _data_ptr[_index(xi + 2, yi + 2, zi - 1)]) - (_data_ptr[_index(xi + 2, yi , zi + 1)] - _data_ptr[_index(xi + 2, yi , zi - 1)]) - (_data_ptr[_index(xi , yi + 2, zi + 1)] - _data_ptr[_index(xi , yi + 2, zi - 1)]) + (_data_ptr[_index(xi , yi , zi + 1)] - _data_ptr[_index(xi , yi , zi - 1)])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 1, zi + 2)] - _data_ptr[_index(xi + 1, yi + 1, zi )]) - (_data_ptr[_index(xi + 1, yi - 1, zi + 2)] - _data_ptr[_index(xi + 1, yi - 1, zi )]) - (_data_ptr[_index(xi - 1, yi + 1, zi + 2)] - _data_ptr[_index(xi - 1, yi + 1, zi )]) + (_data_ptr[_index(xi - 1, yi - 1, zi + 2)] - _data_ptr[_index(xi - 1, yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 1, zi + 2)] - _data_ptr[_index(xi + 2, yi + 1, zi )]) - (_data_ptr[_index(xi + 2, yi - 1, zi + 2)] - _data_ptr[_index(xi + 2, yi - 1, zi )]) - (_data_ptr[_index(xi , yi + 1, zi + 2)] - _data_ptr[_index(xi , yi + 1, zi )]) + (_data_ptr[_index(xi , yi - 1, zi + 2)] - _data_ptr[_index(xi , yi - 1, zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 1, yi + 2, zi + 2)] - _data_ptr[_index(xi + 1, yi + 2, zi )]) - (_data_ptr[_index(xi + 1, yi , zi + 2)] - _data_ptr[_index(xi + 1, yi , zi )]) - (_data_ptr[_index(xi - 1, yi + 2, zi + 2)] - _data_ptr[_index(xi - 1, yi + 2, zi )]) + (_data_ptr[_index(xi - 1, yi , zi + 2)] - _data_ptr[_index(xi - 1, yi , zi )])),
          0.125 * (1.0/(_spacingx*_spacingy*_spacingz))*((_data_ptr[_index(xi + 2, yi + 2, zi + 2)] - _data_ptr[_index(xi + 2, yi + 2, zi )]) - (_data_ptr[_index(xi + 2, yi , zi + 2)] - _data_ptr[_index(xi + 2, yi , zi )]) - (_data_ptr[_index(xi , yi + 2, zi + 2)] - _data_ptr[_index(xi , yi + 2, zi )]) + (_data_ptr[_index(xi , yi , zi + 2)] - _data_ptr[_index(xi , yi , zi )]));
          // Convert voxel values and partial derivatives to interpolation coefficients.
          _coefs = _C * x;

    }

    // Evaluate the interpolation within this grid voxel.
    dx = dx/_spacingx;
    dy = dy/_spacingy;
    dz = dz/_spacingz;
    int ijkn(0);
    fptype dzpow(1);
    for (int k = 0; k < 4; ++k)
    {
      fptype dypow(1);
      for (int j = 0; j < 4; ++j)
      {
        result += dypow * dzpow * (_coefs[ijkn] + dx * (_coefs[ijkn + 1] + dx * (_coefs[ijkn + 2] + dx * _coefs[ijkn + 3])));
        // std::cout<<"result is :"<<result<<std::endl;
        ijkn += 4;
        dypow *= dy;
      }
      dzpow *= dz;
    }

  }

  if (result>255 )
  {
    result = 255;
  }
  if (result<0 )
  {
    result = 0;
  }

  return result;

}

PYBIND11_MODULE(tricubic, m)
{
  m.doc() = "Tricubic interpolation module for python.";

  py::class_<TriCubicInterpolator>(m, "tricubic")
    .def(py::init<py::list, py::list, py::list, py::list, py::list>())
    .def("ip", &TriCubicInterpolator::ip);
}
