#include <string>
#include <sstream>
#include <Eigen/Dense>
#include <pybind11/pybind11.h>

namespace py = pybind11;

typedef double fptype;

#ifndef TRI_CUBIC_INTERPOLATOR_H
#define TRI_CUBIC_INTERPOLATOR_H

//This code is adapted from https://github.com/deepzot/likely
class TriCubicInterpolator
{
  // Performs tri-cubic interpolation within a 3D periodic grid.
  // Based on http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.89.7835
public:
  TriCubicInterpolator(py::list data, py::list data_x, py::list data_y, py::list data_z, py::list npoints);
  ~TriCubicInterpolator();
  fptype ip(py::list xyz);
private:
  fptype *_data_ptr;
  fptype *_data_ptrx;
  fptype *_data_ptry;
  fptype *_data_ptrz;
  fptype _spacingx;
  fptype _spacingy;
  fptype _spacingz;
  int _n1, _n2, _n3;
  int _i1, _i2, _i3;
  bool _initialized;
  Eigen::Matrix<fptype, 64, 1> _coefs;
  Eigen::Matrix<fptype, 64, 64> _C;
  inline int _index(int i1, int i2, int i3) const
  {
    return i1 + _n1 * (i2 + _n2 * i3);
  }

  inline int _ijk2n(int i1, int i2, int i3) const
  {
    return i1 + 4 * i2 + 16 * i3;
  }

};

#endif
